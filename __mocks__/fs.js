import path from 'path';

const fs = jest.createMockFromModule('fs');

let mockFiles = Object.create(null);

// This is a custom function that our tests can use during setup to specify
// what the files on the "mock" filesystem should look like when any of the
// `fs` APIs are used.
// eslint-disable-next-line @typescript-eslint/naming-convention
function __setMockFiles(newMockFiles) {
  mockFiles = Object.create(null);
  newMockFiles.forEach((file) => {
    const dir = path.dirname(file.name);

    if (!mockFiles[dir]) {
      mockFiles[dir] = [];
    }
    mockFiles[dir].push({ ...file, name: path.basename(file.name) });
  });
}

// A custom version of `readdirSync` that reads from the special mocked out
// file list set via __setMockFiles
function readdirSync(directoryPath) {
  return mockFiles[directoryPath] || [];
}

fs.__setMockFiles = __setMockFiles;
fs.readdirSync = readdirSync;

export default fs;
module.exports = fs;
