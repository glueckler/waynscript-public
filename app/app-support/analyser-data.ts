import { AnalProps } from '../utils/analysers/AnalProps';
import { DataConfig, DataFiles } from './data-config';

export interface IAnalyserStoredData {
  [key: string]: { [key: string]: AnalProps };
}

export class AnalyserData extends DataConfig {
  public analyserData: IAnalyserStoredData = {};

  constructor() {
    super(DataFiles.MAIN_DATA);

    this.data = this.parseDataFile();
    this.analyserData = this.data.analyserData;
  }

  /**
   * getFileAnalysis
   */
  public getFileAnalysis(dirname: string, filename: string): AnalProps {
    const cachedDirAnalysis = this.analyserData[dirname];
    return cachedDirAnalysis && cachedDirAnalysis[filename];
  }

  /**
   * setFileAnalysis
   */
  public setFileAnalysis(dirname: string, filename: string, anal: AnalProps) {
    if (!this.analyserData[dirname]) this.analyserData[dirname] = {};
    this.analyserData[dirname][filename] = anal;
  }

  /**
   * save
   */
  public save() {
    this.writeDataFile(this.data);
  }
}
