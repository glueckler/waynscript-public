import { IAnalyserStoredData } from '../utils/analysers/analyser-cache';

const path = require('path');
const fs = require('fs');
const electron = require('electron');

export enum DataFiles {
  MAIN_DATA = 'app_cache.json',
}

export interface IStoredData {
  analyserData: IAnalyserStoredData;
}

const DEFAULT_DATA: IStoredData = {
  analyserData: {},
};

export class DataConfig {
  // Renderer process has to get `app` module via `remote`, whereas the main process can get it directly
  // app.getPath('userData') will return a string of the user's app data directory path.
  public masterDataDirectory = (electron.app || electron.remote.app).getPath(
    'userData'
  );

  public filepath = '';

  public data = {} as IStoredData;

  constructor(public dataFile: DataFiles) {
    this.filepath = path.join(this.masterDataDirectory, this.dataFile);
  }

  parseDataFile() {
    // We'll try/catch it in case the file doesn't exist yet, which will be the case on the first application run.
    try {
      // `fs.readFileSync` will return a JSON string which we then parse into a Javascript object
      return JSON.parse(fs.readFileSync(this.filepath));
    } catch (error) {
      console.warn(
        `Cache file not parsed successfully, it might not exist yet: ${error}`
      );
      this.writeDataFile(DEFAULT_DATA);
      return DEFAULT_DATA;
    }
  }

  writeDataFile(data: IStoredData) {
    // todo: try catch this
    try {
      fs.writeFileSync(this.filepath, JSON.stringify(data));
    } catch (err) {
      console.warn(`problem writing data..... ${err}`);
    }
  }
}
