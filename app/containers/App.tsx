import React, { ReactNode } from 'react';
import { useIPC } from './IPC/useIPC';

type Props = {
  children: ReactNode;
};

export default function App(props: Props) {
  useIPC();
  const { children } = props;
  return <>{children}</>;
}
