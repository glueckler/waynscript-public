import { Dispatch } from 'redux';
import { addBpmAnalFromWorker } from '../../features/logic/bpmAnal/bpmAnalSlice';
import { addKeyAnalFromWorker } from '../../features/logic/keyAnal/keyAnalSlice';
import { updateBpmsWithNewAnalysises } from '../../features/view/filebrowse/filebrowseSlice';
import { IWorkerResponseData } from './IPC.types';

export const receiveBPMAnalChunk = (dispatch: Dispatch) => (
  event: any,
  payload: string
) => {
  const payloadParsed: IWorkerResponseData = JSON.parse(payload);
  const { browsedFilesAnal, bpmAnal, keyAnal } = payloadParsed;

  dispatch(addBpmAnalFromWorker({ ...bpmAnal }));
  dispatch(addKeyAnalFromWorker({ ...keyAnal }));
  dispatch(updateBpmsWithNewAnalysises({ ...browsedFilesAnal }));
};
