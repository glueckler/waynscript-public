import { IBpmAnalState } from '../../features/logic/bpmAnal/bpmAnalSlice';
import { IKeyAnalState } from '../../features/logic/keyAnal/keyAnalSlice';
import { IBrowsedFileMap } from '../../features/view/filebrowse/filebrowseSlice';

export interface IIPCToWorkerForAnal {
  browsedFiles: IBrowsedFileMap;
}

export interface IWorkerResponseData {
  bpmAnal: IBpmAnalState;
  keyAnal: IKeyAnalState;
  browsedFilesAnal: IBrowsedFileMap;
}
