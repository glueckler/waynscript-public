import { createSlice } from '@reduxjs/toolkit';

export interface IIPCState {
  working: boolean;
}

const initialState: IIPCState = {
  working: false,
};

const IPCslice = createSlice({
  name: 'IPC',
  initialState,
  reducers: {
    setIPCWorkingStateToWorking: (state) => {
      state.working = true;
    },
    setIPCWorkingStateToNotWorking: (state) => {
      state.working = false;
    },
  },
});

export default IPCslice.reducer;

export const {
  setIPCWorkingStateToWorking,
  setIPCWorkingStateToNotWorking,
} = IPCslice.actions;
