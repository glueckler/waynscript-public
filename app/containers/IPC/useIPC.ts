import { ipcRenderer } from 'electron';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { workerDoesntNeedRefresh } from '../../features/view/filebrowse/filebrowseSlice';
import { RootState } from '../../store';
import { ALL_UNANALYZED, RETURNING_ANAL_FILES } from '../../utils/ipc/ipcMsgs';
import { messageWorker } from '../../utils/ipc/renderer';
import { receiveBPMAnalChunk } from './IPC.functions';
import { IIPCToWorkerForAnal } from './IPC.types';

export const useIPC = () => {
  const dispatch = useDispatch();
  const filebrowse = useSelector((state: RootState) => state.filebrowse);

  useEffect(() => {
    if (!filebrowse.refreshWorker) return;

    const sendingPayload: IIPCToWorkerForAnal = {
      browsedFiles: filebrowse.browsedFiles,
    };

    const jsonPayload = JSON.stringify(sendingPayload);

    messageWorker(ALL_UNANALYZED, jsonPayload);

    dispatch(workerDoesntNeedRefresh());
  }, [filebrowse.browsedFiles, filebrowse.refreshWorker, dispatch]);

  // set up an ipc listener.. important to only do this once during component life
  useEffect(() => {
    ipcRenderer.on(RETURNING_ANAL_FILES, receiveBPMAnalChunk(dispatch));
  }, [dispatch]);
};
