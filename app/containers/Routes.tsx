/* eslint react/jsx-props-no-spreading: off */
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import routes from '../constants/routes.json';
import { MainView } from '../features/view/MainView';
import App from './App';

export default function Routes() {
  return (
    <App>
      <Switch>
        <Route path={routes.HOME} component={MainView} />
      </Switch>
    </App>
  );
}
