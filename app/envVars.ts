import * as dotenv from 'dotenv';

dotenv.config({ path: `${__dirname}/../.env` });

export const { MASTER_SAMPLE_DIR } = process.env;
console.log(`process.env.MASTER_SAMPLE_DIR ==> ${MASTER_SAMPLE_DIR}`);
