import { IFileMap } from '../../../utils/common.types';

export interface IBpmByNumber {
  [key: number]: IFileMap;
}
