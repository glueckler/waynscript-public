import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import _ from 'lodash';
import { Result } from '../../../utils/common.types';
import { IBpmByNumber } from './IBpmByNumber';
import initBpmByNumber from './initBpmByNumber';

export interface IBpmAnalState {
  [Result.CERTAIN_FIND]: IBpmByNumber;
  [Result.UNCERTAIN_FIND]: IBpmByNumber;
}

export const bpmAnalInitState: IBpmAnalState = {
  [Result.CERTAIN_FIND]: initBpmByNumber,
  [Result.UNCERTAIN_FIND]: initBpmByNumber,
};

const bpmAnalSlice = createSlice({
  name: 'BPM Analysis',
  initialState: bpmAnalInitState,
  reducers: {
    addBpmAnalFromWorker: (state, action: PayloadAction<IBpmAnalState>) => {
      const { payload } = action;
      return {
        ...state,
        ...payload,
      };
    },
  },
});

export const { addBpmAnalFromWorker } = bpmAnalSlice.actions;

export default bpmAnalSlice.reducer;
