import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../store';
import { addUnanalFilesToBpm } from '../bpmAnal/bpmAnalSlice';

export const useUnanalyzedFileSync = () => {
  const filebrowse = useSelector((state: RootState) => state.filebrowse);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(addUnanalFilesToBpm(filebrowse));
  }, [dispatch, filebrowse]);
};
