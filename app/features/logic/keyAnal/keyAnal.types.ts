import { KeyKeys } from '../../../utils/analysers/KeyAnal';
import { IFileMap } from '../../../utils/common.types';

export interface IFileMapByKeys {
  [KeyKeys.NULL_KEY]: IFileMap;
  [KeyKeys.A_FLAT_MAJ]: IFileMap;
  [KeyKeys.A_FLAT_MIN]: IFileMap;
  [KeyKeys.A_NAT_MAJ]: IFileMap;
  [KeyKeys.A_NAT_MIN]: IFileMap;
  [KeyKeys.B_FLAT_MAJ]: IFileMap;
  [KeyKeys.B_FLAT_MIN]: IFileMap;
  [KeyKeys.B_NAT_MAJ]: IFileMap;
  [KeyKeys.B_NAT_MIN]: IFileMap;
  [KeyKeys.C_NAT_MAJ]: IFileMap;
  [KeyKeys.C_NAT_MIN]: IFileMap;
  [KeyKeys.D_FLAT_MAJ]: IFileMap;
  [KeyKeys.D_FLAT_MIN]: IFileMap;
  [KeyKeys.D_NAT_MAJ]: IFileMap;
  [KeyKeys.D_NAT_MIN]: IFileMap;
  [KeyKeys.E_FLAT_MAJ]: IFileMap;
  [KeyKeys.E_FLAT_MIN]: IFileMap;
  [KeyKeys.E_NAT_MAJ]: IFileMap;
  [KeyKeys.E_NAT_MIN]: IFileMap;
  [KeyKeys.F_NAT_MAJ]: IFileMap;
  [KeyKeys.F_NAT_MIN]: IFileMap;
  [KeyKeys.G_FLAT_MAJ]: IFileMap;
  [KeyKeys.G_FLAT_MIN]: IFileMap;
  [KeyKeys.G_NAT_MAJ]: IFileMap;
  [KeyKeys.G_NAT_MIN]: IFileMap;
}
