import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Result } from '../../../utils/common.types';
import { initKeyByKey } from './initKeyByKey';
import { IFileMapByKeys } from './keyAnal.types';

export interface IKeyAnalState {
  [Result.CERTAIN_FIND]: IFileMapByKeys;
  [Result.UNCERTAIN_FIND]: IFileMapByKeys;
}

export const keyAnalInitState: IKeyAnalState = {
  [Result.CERTAIN_FIND]: initKeyByKey,
  [Result.UNCERTAIN_FIND]: initKeyByKey,
};

const keyAnalSlice = createSlice({
  name: 'Key Analysis slice',
  initialState: keyAnalInitState,
  reducers: {
    addKeyAnalFromWorker: (state, action: PayloadAction<IKeyAnalState>) => {
      const { payload } = action;
      return {
        ...state,
        ...payload,
      };
    },
  },
});

export default keyAnalSlice.reducer;

export const { addKeyAnalFromWorker } = keyAnalSlice.actions;
