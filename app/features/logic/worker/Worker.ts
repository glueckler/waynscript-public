import { BpmAnal, IBpm } from '../../../utils/analysers/BpmAnal';
import { IKey, KeyAnal } from '../../../utils/analysers/KeyAnal';
import { RETURNING_ANAL_FILES } from '../../../utils/ipc/ipcMsgs';
import { messageRenderer } from '../../../utils/ipc/worker';
import { AnalPhase } from '../../view/filebrowse/filebrowseSlice';
import { IPhase1Anal } from './worker.types';
import { WorkerResponse } from './WorkerResponse';
import { WorkerState } from './WorkerState';

const MAX_PHASE0_ANALYSIS_CHUNK = 50000;

export class Worker {
  private idle = true;

  private timer: ReturnType<typeof setTimeout> = setTimeout(() => {}, 1);

  constructor(private workerState: WorkerState) {}

  get isIdle() {
    return this.idle;
  }

  /**
   * sendAnalyzedFilesToView
   */
  public sendAnalyzedFilesToView(): void {
    const workerResponse = new WorkerResponse(this.workerState);

    const returnPayload = workerResponse.response();

    const stringPayload = JSON.stringify(returnPayload);

    messageRenderer(RETURNING_ANAL_FILES, stringPayload);
  }

  /**
   * analyze
   */
  public analyze() {
    console.log(
      `\nstarting analysis.. Unanalyzed ie PHASE0 count: ${this.workerState.unAnalyzedFileCount}`
    );

    // start analyzing phase0..
    const phase0 = this.workerState.getAnalPhase0();

    const filenames = Object.keys(phase0);
    console.log(`Total of ${filenames.length} PHASE0 samples left\n`);
    const filenamesChunk = filenames.slice(0, MAX_PHASE0_ANALYSIS_CHUNK);

    filenamesChunk.forEach((filename) => {
      const bpm = new BpmAnal(filename);
      const key = new KeyAnal(filename);
      const newPhase1Anal = this.newPhase1Analysis(filename, bpm.bpm, key.key);
      this.workerState.moveFileFromP0toP1(newPhase1Anal);
    });

    // send analyzed files back after each analysis?
    this.sendAnalyzedFilesToView();

    // analyze other phases in the future here..

    if (this.workerState.unAnalyzedFileCount > 0) {
      this.timer = setTimeout(() => {
        this.analyze();
      }, 5000);
    }
  }

  private newPhase1Analysis(path: string, bpm: IBpm, key: IKey): IPhase1Anal {
    return {
      path,
      bpm,
      key,
      analPhase: AnalPhase.PHASE1,
    };
  }

  /**
   * clearFutureAnalysis
   */
  public clearFutureAnalysis() {
    clearTimeout(this.timer);
  }
}
