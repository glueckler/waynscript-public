import { WorkerState } from './WorkerState';
import { WorkerResponse } from './WorkerResponse';
import { Result } from '../../../utils/common.types';
import initBpmByNumber from '../bpmAnal/initBpmByNumber';
import { IWorkerResponseData } from '../../../containers/IPC/IPC.types';
import { AnalPhase } from '../../view/filebrowse/filebrowseSlice';
import { KeyKeys } from '../../../utils/analysers/KeyAnal';
import { initKeyByKey } from '../keyAnal/initKeyByKey';

describe('Worker Response', () => {
  describe('After running the constructor', () => {
    it('should organize the response correctly', () => {
      const workerState = new WorkerState();
      workerState.analyzed = {
        [AnalPhase.PHASE0]: {
          '92 BPM 100 Guitar Loops': { path: '92 BPM 100 Guitar Loops' },
          '92 bpm Guitar Loops 85': { path: '92 bpm Guitar Loops 85' },
          'EFL-S2-92bpm-F#_68_Classical': {
            path: 'EFL-S2-92bpm-F#_68_Classical',
          },
        },
        [AnalPhase.PHASE1]: {
          'Emotionz 99 92bpm': {
            path: 'Emotionz 99 92bpm',
            bpm: {
              bpm: 92,
              result: Result.CERTAIN_FIND,
            },
            key: { key: KeyKeys.NULL_KEY, result: Result.NO_FIND },
            analPhase: AnalPhase.PHASE1,
          },
          'key amin nobpm': {
            path: 'key amin nobpm',
            bpm: {
              bpm: 0,
              result: Result.NO_FIND,
            },
            key: { key: KeyKeys.A_NAT_MIN, result: Result.CERTAIN_FIND },
            analPhase: AnalPhase.PHASE1,
          },
        },
      };

      const expectedResponse: IWorkerResponseData = {
        browsedFilesAnal: {
          'Emotionz 99 92bpm': {
            path: 'Emotionz 99 92bpm',
            bpm: {
              bpm: 92,
              result: Result.CERTAIN_FIND,
            },
            key: { key: KeyKeys.NULL_KEY, result: Result.NO_FIND },
            analPhase: AnalPhase.PHASE1,
          },
          'key amin nobpm': {
            path: 'key amin nobpm',
            bpm: {
              bpm: 0,
              result: Result.NO_FIND,
            },
            key: { key: KeyKeys.A_NAT_MIN, result: Result.CERTAIN_FIND },
            analPhase: AnalPhase.PHASE1,
          },
        },
        bpmAnal: {
          [Result.CERTAIN_FIND]: {
            ...initBpmByNumber,
            92: {
              'Emotionz 99 92bpm': {
                path: 'Emotionz 99 92bpm',
              },
            },
          },
          [Result.UNCERTAIN_FIND]: {
            ...initBpmByNumber,
          },
        },
        keyAnal: {
          [Result.CERTAIN_FIND]: {
            ...initKeyByKey,
            [KeyKeys.A_NAT_MIN]: {
              'key amin nobpm': {
                path: 'key amin nobpm',
              },
            },
          },
          [Result.UNCERTAIN_FIND]: {
            ...initKeyByKey,
          },
        },
      };

      const workerResponse = new WorkerResponse(workerState);

      expect(workerResponse.response()).toEqual(expectedResponse);
    });
  });
});
