//  this may not need to be a class..

import { IWorkerResponseData } from '../../../containers/IPC/IPC.types';
import { Result } from '../../../utils/common.types';
import {
  AnalPhase,
  IBrowsedFileMap,
} from '../../view/filebrowse/filebrowseSlice';
import { bpmAnalInitState, IBpmAnalState } from '../bpmAnal/bpmAnalSlice';
import { IKeyAnalState, keyAnalInitState } from '../keyAnal/keyAnalSlice';
import { IPhase1Anal } from './worker.types';
import { WorkerState } from './WorkerState';

export class WorkerResponse implements IWorkerResponseData {
  public bpmAnal: IBpmAnalState = bpmAnalInitState;

  public keyAnal: IKeyAnalState = keyAnalInitState;

  public browsedFilesAnal: IBrowsedFileMap = {};

  constructor(workerState: WorkerState) {
    // build up using only files that have been analyzed.. ie that have at least PHASE1
    Object.values(workerState.analyzed.PHASE1).forEach((analysis) => {
      // first add it to the browsed file analysises
      this.addPhase1AnalysisToBrowsedFileResponse(analysis);

      // now add it to the correct bpm anal directory
      this.addPhase1AnalysisToAnalyzedResponse(analysis);
    });
  }

  private addPhase1AnalysisToBrowsedFileResponse({
    bpm,
    key,
    path,
  }: IPhase1Anal): void {
    this.browsedFilesAnal[path] = {
      path,
      bpm,
      key,
      analPhase: AnalPhase.PHASE1,
    };
  }

  private addPhase1AnalysisToAnalyzedResponse({
    bpm,
    key,
    path,
  }: IPhase1Anal): void {
    if (bpm.result === Result.CERTAIN_FIND) {
      this.bpmAnal[Result.CERTAIN_FIND][bpm.bpm][path] = { path };
    }
    if (bpm.result === Result.UNCERTAIN_FIND) {
      this.bpmAnal[Result.UNCERTAIN_FIND][bpm.bpm][path] = { path };
    }

    if (key.result === Result.CERTAIN_FIND) {
      this.keyAnal[Result.CERTAIN_FIND][key.key][path] = { path };
    }
    if (key.result === Result.UNCERTAIN_FIND) {
      this.keyAnal[Result.UNCERTAIN_FIND][key.key][path] = { path };
    }
  }

  /**
   * response
   */
  public response(): IWorkerResponseData {
    return {
      bpmAnal: this.bpmAnal,
      keyAnal: this.keyAnal,
      browsedFilesAnal: this.browsedFilesAnal,
    };
  }
}
