import { KeyKeys } from '../../../utils/analysers/KeyAnal';
import { Result } from '../../../utils/common.types';
import {
  AnalPhase,
  IBrowsedFileMap,
} from '../../view/filebrowse/filebrowseSlice';
import { IPhase1Anal } from './worker.types';
import { WorkerState } from './WorkerState';

describe('WorkerState', () => {
  describe('updating browsed files', () => {
    it('Should replace the browsed files in state', () => {
      const browseFilesFromView: IBrowsedFileMap = {
        '92 BPM 100 Guitar Loops': {
          bpm: { bpm: 0, result: Result.NOT_YET_ANAL },
          key: { key: KeyKeys.NULL_KEY, result: Result.NOT_YET_ANAL },
          path: '92 BPM 100 Guitar Loops',
          analPhase: AnalPhase.PHASE0,
        },
        '92 bpm Guitar Loops 85': {
          bpm: { bpm: 0, result: Result.NOT_YET_ANAL },
          key: { key: KeyKeys.NULL_KEY, result: Result.NOT_YET_ANAL },
          path: '92 bpm Guitar Loops 85',
          analPhase: AnalPhase.PHASE0,
        },
        'EFL-S2-92bpm-F#_68_Classical': {
          bpm: { bpm: 0, result: Result.NOT_YET_ANAL },
          key: { key: KeyKeys.NULL_KEY, result: Result.NOT_YET_ANAL },
          path: 'EFL-S2-92bpm-F#_68_Classical',
          analPhase: AnalPhase.PHASE0,
        },
        'Emotionz 99 92bpm': {
          bpm: { bpm: 0, result: Result.NOT_YET_ANAL },
          key: { key: KeyKeys.NULL_KEY, result: Result.NOT_YET_ANAL },
          path: 'Emotionz 99 92bpm',
          analPhase: AnalPhase.PHASE0,
        },
      };

      const workerState = new WorkerState();
      workerState.updateBrowsedFiles(browseFilesFromView);

      expect(workerState.browsedFiles).toEqual(browseFilesFromView);
    });

    it('should place pre analyzed files in the right spot..', () => {
      const preAnalyzed: IBrowsedFileMap = {
        'Emotionz 99 92bpm': {
          path: 'Emotionz 99 92bpm',
          bpm: {
            bpm: 92,
            result: Result.CERTAIN_FIND,
          },
          key: { key: KeyKeys.NULL_KEY, result: Result.NO_FIND },
          analPhase: AnalPhase.PHASE1,
        },
      };

      const browseFilesFromView: IBrowsedFileMap = {
        '92 BPM 100 Guitar Loops': {
          bpm: { bpm: 0, result: Result.NOT_YET_ANAL },
          key: { key: KeyKeys.NULL_KEY, result: Result.NOT_YET_ANAL },
          path: '92 BPM 100 Guitar Loops',
          analPhase: AnalPhase.PHASE0,
        },
        '92 bpm Guitar Loops 85': {
          bpm: { bpm: 0, result: Result.NOT_YET_ANAL },
          key: { key: KeyKeys.NULL_KEY, result: Result.NOT_YET_ANAL },
          path: '92 bpm Guitar Loops 85',
          analPhase: AnalPhase.PHASE0,
        },
        'EFL-S2-92bpm-F#_68_Classical': {
          bpm: { bpm: 0, result: Result.NOT_YET_ANAL },
          key: { key: KeyKeys.NULL_KEY, result: Result.NOT_YET_ANAL },
          path: 'EFL-S2-92bpm-F#_68_Classical',
          analPhase: AnalPhase.PHASE0,
        },
        ...preAnalyzed,
      };

      const workerState = new WorkerState();
      workerState.updateBrowsedFiles(browseFilesFromView);
      workerState.organizeBrowsedFiles();

      expect(workerState.browsedFiles).toEqual(browseFilesFromView);
      expect(workerState.analyzed[AnalPhase.PHASE1]).toEqual(preAnalyzed);
    });
  });

  describe('Moving a files from one state to another', () => {
    it('should move a file from phase0 to phase1', () => {
      const preAnalyzed: IBrowsedFileMap = {
        'Emotionz 99 92bpm': {
          path: 'Emotionz 99 92bpm',
          bpm: {
            bpm: 92,
            result: Result.CERTAIN_FIND,
          },
          key: { key: KeyKeys.NULL_KEY, result: Result.NO_FIND },
          analPhase: AnalPhase.PHASE1,
        },
      };

      const browseFilesFromView: IBrowsedFileMap = {
        '92 BPM 100 Guitar Loops': {
          bpm: { bpm: 0, result: Result.NOT_YET_ANAL },
          key: { key: KeyKeys.NULL_KEY, result: Result.NOT_YET_ANAL },
          path: '92 BPM 100 Guitar Loops',
          analPhase: AnalPhase.PHASE0,
        },
        ...preAnalyzed,
      };

      const nextPhase1Anal: IPhase1Anal = {
        bpm: { bpm: 92, result: Result.CERTAIN_FIND },
        key: { key: KeyKeys.NULL_KEY, result: Result.NO_FIND },
        path: '92 BPM 100 Guitar Loops',
        analPhase: AnalPhase.PHASE1,
      };

      const workerState = new WorkerState();
      workerState.updateBrowsedFiles(browseFilesFromView);
      workerState.organizeBrowsedFiles();

      workerState.moveFileFromP0toP1(nextPhase1Anal);

      expect(workerState.analyzed.PHASE0).toEqual({});
      expect(workerState.analyzed.PHASE1).toEqual({
        ...preAnalyzed,
        '92 BPM 100 Guitar Loops': nextPhase1Anal,
      });
    });
  });
});
