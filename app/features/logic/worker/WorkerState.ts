import { IFileMap } from '../../../utils/common.types';
import {
  AnalPhase,
  IBrowsedFileMap,
} from '../../view/filebrowse/filebrowseSlice';
import { IPhase1Anal } from './worker.types';

export interface IPhase1List {
  [key: string]: IPhase1Anal;
}

type IPhase0List = IFileMap;

export class WorkerState {
  public browsedFiles: IBrowsedFileMap = {};

  public analyzed: {
    [AnalPhase.PHASE0]: IPhase0List;
    [AnalPhase.PHASE1]: IPhase1List;
  } = {
    [AnalPhase.PHASE0]: {},
    [AnalPhase.PHASE1]: {},
  };

  /**
   * updateBrowsedFiles
   */
  public updateBrowsedFiles(filesFromView: IBrowsedFileMap): void {
    this.browsedFiles = filesFromView;
  }

  /**
   * clearUnanalyzedFiles
   */
  public clearUnanalyzedFiles() {
    this.analyzed.PHASE0 = {};
  }

  /**
   * organizeBrowsedFiles
   */
  public organizeBrowsedFiles() {
    Object.values(this.browsedFiles).forEach((browsedFile) => {
      const { analPhase, path } = browsedFile;
      this.analyzed[analPhase][path] = browsedFile;
    });
  }

  /**
   * getAllPhase0
   */
  public getAnalPhase0(): IPhase0List {
    return this.analyzed[AnalPhase.PHASE0];
  }

  /**
   * getAnalPhase1
   */
  public getAnalPhase1(): IPhase1List {
    return this.analyzed[AnalPhase.PHASE1];
  }

  /**
   * moveFileFromP0toP1
   */
  public moveFileFromP0toP1(phase1Anal: IPhase1Anal): void {
    this.analyzed[AnalPhase.PHASE1][phase1Anal.path] = phase1Anal;
    delete this.analyzed[AnalPhase.PHASE0][phase1Anal.path];
  }

  /**
   * unAnalyzedFileCount
   */
  public get unAnalyzedFileCount(): number {
    return Object.keys(this.analyzed[AnalPhase.PHASE0]).length;
  }
}
