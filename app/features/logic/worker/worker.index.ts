import { ipcRenderer } from 'electron';
import { IIPCToWorkerForAnal } from '../../../containers/IPC/IPC.types';
import { ALL_UNANALYZED } from '../../../utils/ipc/ipcMsgs';

import { Worker } from './Worker';
import { WorkerState } from './WorkerState';

const workerState = new WorkerState();
const worker = new Worker(workerState);

const handleAllUnanalyzed = (event: any, payload: string) => {
  // stop analysis, we will restart it soon..
  worker.clearFutureAnalysis();

  const { browsedFiles }: IIPCToWorkerForAnal = JSON.parse(payload);
  workerState.updateBrowsedFiles(browsedFiles);
  workerState.clearUnanalyzedFiles();
  workerState.organizeBrowsedFiles();

  console.log(
    `\n\nUpdated And Organized browsed files with ${
      Object.keys(browsedFiles).length
    } files.. some may already be analyzed..`
  );
  // console.log(JSON.stringify(browsedFiles, undefined, 2));

  worker.analyze();
};

export function initWorker(): void {
  console.log('Worker Initialized');

  ipcRenderer.on(ALL_UNANALYZED, handleAllUnanalyzed);
}
