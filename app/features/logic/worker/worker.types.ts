import { IBpm } from '../../../utils/analysers/BpmAnal';
import { IKey } from '../../../utils/analysers/KeyAnal';
import { AnalPhase } from '../../view/filebrowse/filebrowseSlice';

export interface IPhase1Anal {
  bpm: IBpm;
  key: IKey;
  path: string;
  analPhase: AnalPhase.PHASE1;
}
