import React from 'react';

import { FilebrowseView } from './filebrowse/FilebrowseView';
import { FileList } from './filelist/FileList';
import { FilterView } from './filter/FilterView';
import styles from './MainView.css';

export function MainView(): JSX.Element {
  return (
    <div className={styles.mainView}>
      <FilebrowseView />
      <FilterView />
      <FileList />
    </div>
  );
}
