import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';

import {
  IChunkOfFiles,
  selectChunksRemaining,
  selectChunkInProgress,
  setChunksRemainingByList,
  setChunkAsInProgress,
  resetChunks,
} from './analyzerStateSlice';
import { resetAnalyzedState } from './analyzedSlice';
import { FilePath } from '../../../utils/FilePath';
import { messageWorker } from '../../../utils/ipc/renderer';
import {
  DirectoryAnalysis,
  IIpcFilesChunk,
} from '../../../utils/analysers/DirectoryAnalysis';
import { PLEASE_ANAL_FILES } from '../../../utils/ipc/ipcMsgs';

import { resetFilteredState } from '../filter/filteredSlice';
import { AnalysisTypes } from './AnalysisTypes';

const { dialog } = require('electron').remote;

const ANALYSIS_CHUNK_SIZE = 1777;

export function AnalyzerView(): JSX.Element {
  const dispatch = useDispatch();
  const [analysedFolder, setAnalysedFolder] = useState('');

  const chunksRemaining = useSelector(selectChunksRemaining);
  const chunkInProgress = useSelector(selectChunkInProgress);

  // effect of changing the master directory of samples.. re-analyze
  useEffect(() => {
    if (!analysedFolder) return;

    console.log('AnalyzerView: RESET ANALYZER FOLDER SELECT');

    dispatch(resetAnalyzedState());
    dispatch(resetFilteredState());
    dispatch(resetChunks());

    const directoryAnalysis = new DirectoryAnalysis(analysedFolder);
    directoryAnalysis.initFileList();

    const chunksOfFiles = createChunksOfFiles(
      directoryAnalysis.recursiveFileList
    );

    dispatch(setChunksRemainingByList(chunksOfFiles));
  }, [analysedFolder, dispatch]);

  useEffect(() => {
    if (chunksRemaining.length === 0) return;

    if (!chunkInProgress) {
      const nextChunk = chunksRemaining[0];
      sendChunkToWorker(nextChunk.chunkName, nextChunk.chunkFiles);
    }
  }, [chunkInProgress, sendChunkToWorker, chunksRemaining]);

  function createChunksOfFiles(fileList: FilePath[]): IChunkOfFiles[] {
    const chunksOfFiles: IChunkOfFiles[] = [];
    _.chunk(fileList, ANALYSIS_CHUNK_SIZE).forEach((chunk, index) => {
      const chunkName = `chunkNum${index}`;
      chunksOfFiles.push({
        chunkName,
        chunkFiles: [...chunk],
      });
    });
    return chunksOfFiles;
  }

  //
  // render..
  //

  function renderAnalysisLoading() {
    return null;
  }

  return <div>{renderAnalysisLoading()}</div>;
}
