import { Dispatch } from 'redux';

import { IFileAnalysis } from '../../../utils/analysers/IFileAnalysis';
import {
  addAnalsToChunksDictionary,
  addAnalsToBpmDictionary,
  addAnalsToKeyDictionary,
  IBpmDictionary,
  IKeysDictionary,
} from './analyzedSlice';

export const addToAnalyzed = (
  fileAnalysisList: IFileAnalysis[],
  chunkName: string,
  dispatch: Dispatch
) => {
  const bpmDictionary: IBpmDictionary = {};
  const keyDictionary: IKeysDictionary = {};
  fileAnalysisList.forEach((fileAnalysis) => {
    const { hasBpm, bpm, hasKey, key } = fileAnalysis;
    if (hasBpm) {
      if (!bpmDictionary[bpm.bpm]) bpmDictionary[bpm.bpm] = [];
      bpmDictionary[bpm.bpm].push(fileAnalysis);
    }
    if (hasKey) {
      if (!keyDictionary[key.key]) keyDictionary[key.key] = [];
      keyDictionary[key.key].push(fileAnalysis);
    }
  });

  dispatch(addAnalsToChunksDictionary({ chunkName, fileAnalysisList }));
  dispatch(addAnalsToBpmDictionary(bpmDictionary));
  dispatch(addAnalsToKeyDictionary(keyDictionary));
};
