import { createSlice, PayloadAction } from '@reduxjs/toolkit';
// eslint-disable-next-line import/no-cycle
import { RootState } from '../../../store';
import { IFileAnalysis } from '../../../utils/analysers/IFileAnalysis';

export interface IBpmDictionary {
  [key: string]: IFileAnalysis[];
}

export interface IKeysDictionary {
  [key: string]: IFileAnalysis[];
}

export interface IChunksDictionary {
  [key: string]: IFileAnalysis[];
}

export interface IFilterDictionaryState {
  chunks: IChunksDictionary;
  bpms: IBpmDictionary;
  keys: IKeysDictionary;
}

const initialState: IFilterDictionaryState = {
  chunks: {},
  bpms: {},
  keys: {},
};

const analyzedSlice = createSlice({
  name: 'Analyzed Dictionary',
  initialState,
  reducers: {
    addAnalsToChunksDictionary: (
      state,
      action: PayloadAction<{
        chunkName: string;
        fileAnalysisList: IFileAnalysis[];
      }>
    ) => {
      state.chunks[action.payload.chunkName] = action.payload.fileAnalysisList;
    },
    addAnalsToBpmDictionary: (state, action: PayloadAction<IBpmDictionary>) => {
      const bpmKeys = Object.keys(action.payload);
      bpmKeys.forEach((bpm) => {
        if (!state.bpms[bpm]) state.bpms[bpm] = [];
        state.bpms[bpm] = state.bpms[bpm].concat(action.payload[bpm]);
      });
    },
    addAnalsToKeyDictionary: (
      state,
      action: PayloadAction<IKeysDictionary>
    ) => {
      const keysKeys = Object.keys(action.payload);
      keysKeys.forEach((key) => {
        if (!state.keys[key]) state.keys[key] = [];
        state.keys[key] = state.keys[key].concat(action.payload[key]);
      });
    },
    resetAnalyzedState: (state) => {
      state.chunks = {};
      state.bpms = {};
      state.keys = {};
    },
  },
});

export const {
  addAnalsToChunksDictionary,
  addAnalsToBpmDictionary,
  addAnalsToKeyDictionary,
  resetAnalyzedState,
} = analyzedSlice.actions;

export default analyzedSlice.reducer;

export const selectAnalyzedState = (state: RootState) => state.analyzedSlice;
export const selectChunksDictionary = (state: RootState) =>
  state.analyzedSlice.chunks;
