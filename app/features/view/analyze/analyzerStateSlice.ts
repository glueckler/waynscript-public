import { createSlice, PayloadAction } from '@reduxjs/toolkit';
// eslint-disable-next-line import/no-cycle
import { RootState } from '../../../store';
import { FilePath } from '../../../utils/FilePath';

export type IChunkOfFiles = {
  chunkName: string;
  chunkFiles: FilePath[];
};

export interface IAnalyzerState {
  chunksRemaining: IChunkOfFiles[];
  chunksComplete: IChunkOfFiles[];
  chunkInProgress: IChunkOfFiles | null;
}

const initialState: IAnalyzerState = {
  chunksComplete: [],
  chunksRemaining: [],
  chunkInProgress: null,
};

const analyzerStateSlice = createSlice({
  name: 'Analyzer State',
  initialState,
  reducers: {
    setChunksRemainingByList: (
      state,
      action: PayloadAction<IChunkOfFiles[]>
    ) => {
      state.chunksRemaining = action.payload;
    },

    setChunkAsFinished: (state, action: PayloadAction<string>) => {
      let chunkToSwap: IChunkOfFiles | null = null;
      if (action.payload !== state.chunkInProgress?.chunkName) {
        throw new Error(
          `Attempt to set ${action.payload} to finished but the current Chunk In Progress is ${state.chunkInProgress?.chunkName}`
        );
      }

      chunkToSwap = state.chunkInProgress;

      state.chunksComplete.push(chunkToSwap);
      state.chunkInProgress = null;
    },

    setChunkAsInProgress: (state, action: PayloadAction<string>) => {
      let chunkToSwap: IChunkOfFiles | null = null;

      state.chunksRemaining = state.chunksRemaining.filter((chunk) => {
        if (chunk.chunkName === action.payload) {
          chunkToSwap = chunk;
          return false;
        }
        return true;
      });

      if (chunkToSwap) {
        state.chunkInProgress = chunkToSwap;
      }
    },

    resetChunks: (state) => {
      state.chunksComplete = [];
      state.chunksRemaining = [];
    },
  },
});

export const {
  setChunksRemainingByList,
  setChunkAsFinished,
  setChunkAsInProgress,
  resetChunks,
} = analyzerStateSlice.actions;

export default analyzerStateSlice.reducer;

export const selectChunksRemaining = (state: RootState) =>
  state.analyzerStateSlice.chunksRemaining;

export const selectChunkInProgress = (state: RootState) =>
  state.analyzerStateSlice.chunkInProgress;
