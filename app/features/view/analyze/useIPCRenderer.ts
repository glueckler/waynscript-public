import { ipcRenderer } from 'electron';
import { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { AnalProps } from '../../../utils/analysers/AnalProps';
import { IIpcFilesChunk } from '../../../utils/analysers/DirectoryAnalysis';
import { FilePropsAnalysis } from '../../../utils/analysers/FilePropAnalysis';
import { RETURNING_ANAL_FILES } from '../../../utils/ipc/ipcMsgs';
import { addToAnalyzed } from './addToAnalyzed';
import { setChunkAsFinished } from './analyzerStateSlice';

export const useIPCRenderer = () => {
  const dispatch = useDispatch();

  const receiveChunkFromWorker = useCallback<
    (chunkName: string, analyzedFiles: AnalProps[]) => void
  >(
    (chunkName: string, analyzedFiles) => {
      console.log(`AnalyzerView: Received Analyzed Files for ${chunkName}`);

      addToAnalyzed(
        analyzedFiles.map((anal) => new FilePropsAnalysis(anal)),
        chunkName,
        dispatch
      );

      dispatch(setChunkAsFinished(chunkName));
    },
    [dispatch]
  );

  // set up an ipc listener.. important to only do this once during component life
  useEffect(() => {
    ipcRenderer.on(RETURNING_ANAL_FILES, (event, payload: string) => {
      const payloadParsed: IIpcFilesChunk = JSON.parse(payload);
      const { chunkName, analyzedFiles } = payloadParsed;
      receiveChunkFromWorker(chunkName, analyzedFiles || []);
    });
  }, [dispatch, receiveChunkFromWorker]);
};
