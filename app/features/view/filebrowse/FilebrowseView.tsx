import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../store';
import {
  addAllChildFiles,
  addParentDirectory,
  removeAllChildFiles,
  removeParentDirectory,
  workerNeedsBrowseFilesRefreshed,
} from './filebrowseSlice';

const { dialog } = require('electron').remote;

export function FilebrowseView(): JSX.Element {
  const filebrowse = useSelector((state: RootState) => state.filebrowse);
  const { parentDirectories, totalFilesBrowsed } = filebrowse;
  const dispatch = useDispatch();

  function handleFolderSelect() {
    const selectedPaths = dialog.showOpenDialogSync(null, {
      properties: ['openDirectory'],
    });
    if (!selectedPaths) return;
    dispatch(addParentDirectory({ path: selectedPaths[0] }));
    dispatch(addAllChildFiles({ path: selectedPaths[0] }));
    dispatch(workerNeedsBrowseFilesRefreshed());
  }

  const handleRemoveFolder = useCallback(
    (path: string) => () => {
      dispatch(removeParentDirectory({ path }));
      dispatch(removeAllChildFiles({ path }));
    },
    [dispatch]
  );

  return (
    <div>
      <div>
        Total Files Browsed: <b>{totalFilesBrowsed}</b>
      </div>
      <div>
        {parentDirectories.map(({ path }) => (
          <div key={path}>
            <div>{path}</div>
            <button type="button" onClick={handleRemoveFolder(path)}>
              Remove These Files
            </button>
          </div>
        ))}
      </div>
      <button type="button" onClick={handleFolderSelect}>
        Add Folder
      </button>
    </div>
  );
}
