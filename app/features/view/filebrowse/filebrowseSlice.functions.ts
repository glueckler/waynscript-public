import path from 'path';
import { IFileMap } from '../../../utils/common.types';
import { allAudioFilesInDir } from '../../../utils/recursiveFileList';

const isParent = (parent: string, child: string) => {
  const relative = path.relative(parent, child);
  const isSubdir =
    relative && !relative.startsWith('..') && !path.isAbsolute(relative);
  return isSubdir;
};

export const findChildFilesInParent = (
  parentDir: string,
  otherDirs: string[]
): IFileMap => {
  const childFiles: IFileMap = {};

  otherDirs.forEach((otherDir) => {
    if (isParent(parentDir, otherDir)) {
      const filesInside = allAudioFilesInDir(otherDir);
      filesInside.forEach((file) => {
        childFiles[file] = { path: file };
      });
    }
  });

  return childFiles;
};
