import { IFileMap } from '../../../utils/common.types';
import { findChildFilesInParent } from './filebrowseSlice.functions';

jest.mock('fs');
describe('filebrowseSlice.functions.ts > findChildFilesInParent', () => {
  const MOCK_FILE_INFO = [
    {
      name: '/users/whateverdir/shouldbedeleted',
      isDirectory() {
        return false;
      },
    },
    {
      name: '/users/whateverdir/shouldbedeletedalso',
      isDirectory() {
        return false;
      },
    },
    {
      name: '/users/whateverdir/one/shouldkeep',
      isDirectory() {
        return false;
      },
    },
    {
      name: '/users/whateverdir/two/shouldkeep',
      isDirectory() {
        return false;
      },
    },
    {
      name: '/users/whateverdir/two/shouldalsokeep',
      isDirectory() {
        return false;
      },
    },
    {
      name: '/users/whateverdir',
      isDirectory() {
        return true;
      },
    },
    {
      name: '/users/whateverdir/one',
      isDirectory() {
        return true;
      },
    },
    {
      name: '/users/whateverdir/two',
      isDirectory() {
        return true;
      },
    },
  ];

  beforeEach(() => {
    // Set up some mocked out file info before each test
    require('fs').__setMockFiles(MOCK_FILE_INFO);
  });

  it('should remove parent directories gracefully', () => {
    // test this manually with redux for now, or figure this out..
    // https://jestjs.io/docs/manual-mocks
    // https://stackoverflow.com/a/49359568

    const removeDirectory = '/users/whateverdir';
    const keepDirs = ['/users/whateverdir/one', '/users/whateverdir/two'];

    const foundChilds = findChildFilesInParent(removeDirectory, keepDirs);

    const expectedResult: IFileMap = {
      '/users/whateverdir/one/shouldkeep': {
        path: '/users/whateverdir/one/shouldkeep',
      },
      '/users/whateverdir/two/shouldkeep': {
        path: '/users/whateverdir/two/shouldkeep',
      },
      '/users/whateverdir/two/shouldalsokeep': {
        path: '/users/whateverdir/two/shouldalsokeep',
      },
    };

    expect(foundChilds).toMatchObject(expectedResult);
  });
});
