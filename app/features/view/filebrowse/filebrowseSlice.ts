/* eslint-disable no-param-reassign */
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import _ from 'lodash';
import { KeyKeys } from '../../../utils/analysers/KeyAnal';
import { IFileMap, Result } from '../../../utils/common.types';
import {
  allAudioFilesInDir,
  audioFileMapOfDir,
} from '../../../utils/recursiveFileList';
import { initFileBrowsed } from './initFileBrowsed';

export enum AnalPhase {
  PHASE0 = 'PHASE0',
  PHASE1 = 'PHASE1',
}

interface IParentDirectories {
  path: string;
}

export interface IBrowsedFile {
  path: string;
  bpm: {
    bpm: number;
    result: Result;
  };
  key: {
    key: KeyKeys;
    result: Result;
  };
  analPhase: AnalPhase;
}

export interface IBrowsedFileMap {
  [key: string]: IBrowsedFile;
}

export interface IFilebrowseState {
  parentDirectories: IParentDirectories[];
  browsedFiles: IBrowsedFileMap;
  refreshWorker: boolean;
  totalFilesBrowsed: number;
}

const initialState: IFilebrowseState = {
  parentDirectories: [],
  browsedFiles: {},
  refreshWorker: false,
  totalFilesBrowsed: 0,
};

const filebrowseSlice = createSlice({
  name: 'Filebrowse',
  initialState,
  reducers: {
    addParentDirectory: (
      state,
      action: PayloadAction<{
        path: string;
      }>
    ) => {
      state.parentDirectories = [
        ...state.parentDirectories,
        { path: action.payload.path },
      ];
    },
    addAllChildFiles: (
      state,
      {
        payload,
      }: PayloadAction<{
        path: string;
      }>
    ) => {
      const allFiles = allAudioFilesInDir(payload.path);

      allFiles.forEach((path) => {
        // check if files have been added in the past..
        if (state.browsedFiles[path]) return;

        state.browsedFiles[path] = initFileBrowsed(path);
      });
      state.totalFilesBrowsed = Object.keys(state.browsedFiles).length;
    },
    removeParentDirectory: (
      state,
      action: PayloadAction<{
        path: string;
      }>
    ) => {
      const prevState = { ...state };

      const parentDirectories = prevState.parentDirectories.filter(
        ({ path }) => path !== action.payload.path
      );

      return {
        ...prevState,
        parentDirectories,
      };
    },
    removeAllChildFiles: (state, action: PayloadAction<{ path: string }>) => {
      const prevState = { ...state };
      const parentDirPaths = prevState.parentDirectories.map(
        (dirs) => dirs.path
      );

      const remainingDirs = parentDirPaths.filter(
        (path) => path !== action.payload.path
      );

      const remainingFiles: IFileMap = remainingDirs.reduce((files, dir) => {
        return Object.assign(files, audioFileMapOfDir(dir));
      }, {});

      state.browsedFiles = _.pick(
        state.browsedFiles,
        Object.keys(remainingFiles)
      );

      state.totalFilesBrowsed = Object.keys(state.browsedFiles).length;

      state.refreshWorker = true;
    },

    workerNeedsBrowseFilesRefreshed: (state) => {
      // use this to indicate the worker needs to refresh its filelist..
      // otherwise there will be a loop of browsedfiles being replaced by the view, and then by the worker then the view again. etc
      state.refreshWorker = true;
    },
    workerDoesntNeedRefresh: (state) => {
      state.refreshWorker = false;
    },
    updateBpmsWithNewAnalysises: (
      state,
      { payload: analyzedFiles }: PayloadAction<IBrowsedFileMap>
    ) => {
      Object.keys(analyzedFiles).forEach((path) => {
        if (state.browsedFiles[path]) {
          state.browsedFiles[path] = analyzedFiles[path];
        }
      });
    },
  },
});

export const {
  addParentDirectory,
  removeParentDirectory,
  removeAllChildFiles,
  addAllChildFiles,
  workerDoesntNeedRefresh,
  workerNeedsBrowseFilesRefreshed,
  updateBpmsWithNewAnalysises,
} = filebrowseSlice.actions;

export default filebrowseSlice.reducer;
