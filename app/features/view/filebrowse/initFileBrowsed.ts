import { KeyKeys } from '../../../utils/analysers/KeyAnal';
import { Result } from '../../../utils/common.types';
import { AnalPhase, IBrowsedFile } from './filebrowseSlice';

export const initFileBrowsed = (path: string): IBrowsedFile => {
  return {
    path,
    bpm: {
      bpm: 0,
      result: Result.NOT_YET_ANAL,
    },
    key: {
      key: KeyKeys.NULL_KEY,
      result: Result.NOT_YET_ANAL,
    },
    analPhase: AnalPhase.PHASE0,
  };
};
