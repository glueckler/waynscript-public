/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import React, { useCallback, useEffect, useState } from 'react';
import { Howl } from 'howler';
import path from 'path';
import { ipcRenderer } from 'electron';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import Toggle from 'react-toggle';

import { IFileAnalysis } from '../../../utils/analysers/IFileAnalysis';
import PlayIcon from '../../../../resources/icons/play.svg.tsx';
import PauseIcon from '../../../../resources/icons/pause.svg.tsx';
import styles from './FileList.css';
import {
  selectAddedFilteredList,
  selectInitialFilteredList,
} from '../filter/filteredSlice';
import { keyVisible } from '../../../utils/KeyDictionary';

const MAX_VISIBLE = 500;
export const FileList = (): JSX.Element => {
  // redux
  const initialFilteredFiles = useSelector(selectInitialFilteredList);
  const addedFilteredFiles = useSelector(selectAddedFilteredList);

  const [randomize, setRandomize] = useState(false);
  const [pool, setPool] = useState<IFileAnalysis[]>([]);
  const [visiblePool, setVisiblePool] = useState<IFileAnalysis[]>([]); // all the visible samples (ordered)

  // howler state
  const [playSamplePath, setPlaySamplePath] = useState<string | null>(null);
  const [playHowler, setPlayHowler] = useState<InstanceType<
    typeof Howl
  > | null>(null);

  //
  // effects
  //

  const addUniqueToPool = useCallback(
    (anals: IFileAnalysis[]) => {
      setPool((prevPool) => {
        if (randomize) {
          return prevPool.concat(
            _.shuffle(anals.filter((anal) => !prevPool.includes(anal)))
          );
        }
        return prevPool.concat(
          _.orderBy(
            anals.filter((anal) => !prevPool.includes(anal)),
            ['filepath'],
            ['asc']
          )
        );
      });
    },
    [randomize]
  );

  const resetPool = useCallback(() => {
    setPool(() => []);
  }, []);

  useEffect(() => {
    console.log('FileList: Initial Filtered Files change detected');
    resetPool();
    addUniqueToPool(initialFilteredFiles);
    // reset visible
    setVisiblePool(() => []);
  }, [initialFilteredFiles, addUniqueToPool, resetPool]);

  useEffect(() => {
    console.log('FileList: Added Filtered Files change detected');
    addUniqueToPool(addedFilteredFiles);
  }, [addedFilteredFiles, addUniqueToPool]);

  useEffect(() => {
    setVisiblePool((prevVisiblePool) => {
      if (prevVisiblePool.length >= MAX_VISIBLE) {
        return prevVisiblePool;
      }

      const notInVisible = pool.filter(
        (anal) => !prevVisiblePool.includes(anal)
      );

      const delta = MAX_VISIBLE - prevVisiblePool.length;

      return prevVisiblePool.concat(notInVisible.slice(0, delta));
    });
  }, [pool]);

  //
  // Howler functions..
  //

  function playSample(filepath: string) {
    const sound = new Howl({
      src: [
        // encoding necessary for file names with sharp hashtag sign
        `${path.resolve('/', ...filepath.split('/').map(encodeURIComponent))}`,
      ],
      onplay: () => {
        setPlaySamplePath(filepath);
        setPlayHowler(sound);
      },
      onstop: () => {
        setPlayHowler(null);
        setPlaySamplePath(null);
      },
      onend: () => {
        setPlayHowler(null);
        setPlaySamplePath(null);
      },
    });

    sound.play();
  }

  //
  // event handlers..
  //

  function handleRefreshList() {
    setVisiblePool(_.shuffle(pool).slice(0, MAX_VISIBLE));
  }

  function handleDragSample(event: React.DragEvent, filepath: string) {
    event.preventDefault();
    ipcRenderer.send('ondragstart', filepath);
  }

  function handleClickSample(event: React.MouseEvent, filepath: string) {
    event.preventDefault();

    // check if there's a howl playing, and clear it if necessary
    if (playHowler) {
      if (filepath === playSamplePath) {
        playHowler?.stop();
      } else {
        playHowler?.stop();
        playSample(filepath);
      }
    } else {
      playSample(filepath);
    }
  }

  //
  // render
  //

  function renderVisiblePool() {
    return (
      <div>
        {visiblePool.map(({ filename, filepath, bpm, key }) => {
          let PlayPauseIcon = PlayIcon;
          if (filepath === playSamplePath) {
            PlayPauseIcon = PauseIcon;
          }
          return (
            <div
              className={styles.fileContainer}
              draggable="true"
              onDragStart={(e) => {
                handleDragSample(e, filepath);
              }}
              onClick={(e) => {
                handleClickSample(e, filepath);
              }}
              key={filepath}
            >
              <div className={styles.playPauseContainer}>
                <PlayPauseIcon />
              </div>
              {filename} - ( {bpm.bpm === 0 ? 'No' : bpm.bpm} Bpm -{' '}
              {keyVisible(key.key)} )
            </div>
          );
        })}
      </div>
    );
  }

  return (
    <div>
      <div>
        <h3>
          Showing {visiblePool.length} of {pool.length}
        </h3>
      </div>
      <div>
        <button onClick={handleRefreshList}>Refresh</button>
        <div>
          <Toggle
            defaultChecked={randomize}
            icons={false}
            onChange={() => setRandomize(!randomize)}
          />
          <span>Randomize</span>
        </div>
      </div>
      {renderVisiblePool()}
    </div>
  );
};
