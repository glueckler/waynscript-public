import React from 'react';
import Chip from '@material-ui/core/Chip';

import {
  TagIds,
  tagDataDictionary,
} from '../../../utils/analysers/tagDataDictionary';
import { IFilterParams } from './runFilter';

interface ICommonTagsProps {
  onClick: (tagId: TagIds) => void;
  filterParams: IFilterParams;
}

export function CommonTags({
  onClick: handleTagClick,
  filterParams,
}: ICommonTagsProps): JSX.Element {
  return (
    <div>
      {Object.values(tagDataDictionary).map((tagData):
        | JSX.Element
        | undefined => {
        if (!tagData) return undefined;
        const { tagId, text } = tagData;

        const isSelected = filterParams.commonTagIds.includes(tagId);
        return (
          <Chip
            key={tagId}
            label={text}
            size="small"
            variant={isSelected ? 'outlined' : 'default'}
            onClick={() => handleTagClick(tagId)}
          />
        );
      })}
    </div>
  );
}
