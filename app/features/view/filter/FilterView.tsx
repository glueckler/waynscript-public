/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useEffect, useState, useReducer } from 'react';
import InputRange from 'react-input-range';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';
import classNames from 'classnames';
import Toggle from 'react-toggle';

import {
  selectAnalyzedState,
  selectChunksDictionary,
} from '../analyze/analyzedSlice';
import { KeyFilterChips } from './KeyFilterChips';
import { runFilter, IFilterParams } from './runFilter';
import { KeyKeys } from '../../../utils/analysers/KeyAnal';
import styles from './FilterView.css';
import usePrevious from '../../../utils/hooks/usePrevious';
import { CommonTags } from './CommonTags';
import { TagIds } from '../../../utils/analysers/tagDataDictionary';

const BPM_PICKER_MIN = 55;
const BPM_PICKER_MAX = 199;

interface IBpmRange {
  min: number;
  max: number;
}

enum FPA {
  SET_BPM_MIN,
  SET_BPM_MAX,
  TOGGLE_KEY,
  TOGGLE_BPM_FILTER,
  TOGGLE_KEYS_FILTER,
  TOGGLE_COMMON_TAGS,
}

type IFilterParamsAction =
  | { type: FPA.SET_BPM_MIN | FPA.SET_BPM_MAX; bpm: number }
  | { type: FPA.TOGGLE_KEY; key: KeyKeys }
  | { type: FPA.TOGGLE_BPM_FILTER | FPA.TOGGLE_KEYS_FILTER }
  | { type: FPA.TOGGLE_COMMON_TAGS; tagId: TagIds };

function filterParamsReducer(
  state: IFilterParams,
  action: IFilterParamsAction
): IFilterParams {
  switch (action.type) {
    case FPA.SET_BPM_MIN:
      return { ...state, bpmMin: action.bpm };
    case FPA.SET_BPM_MAX:
      return { ...state, bpmMax: action.bpm };
    case FPA.TOGGLE_KEY:
      return { ...state, keys: _.xor(state.keys, [action.key]) };
    case FPA.TOGGLE_BPM_FILTER:
      return { ...state, bpmFilterActive: !state.bpmFilterActive };
    case FPA.TOGGLE_KEYS_FILTER:
      return { ...state, keysFilterActive: !state.keysFilterActive };
    case FPA.TOGGLE_COMMON_TAGS:
      return {
        ...state,
        commonTagIds: _.xor(state.commonTagIds, [action.tagId]),
      };
    default:
      throw new Error('Invalid Filter Param action.type');
  }
}

const filtersInitState: IFilterParams = {
  bpmMin: 70,
  bpmMax: 100,
  keys: [],
  bpmFilterActive: false,
  keysFilterActive: false,
  commonTagIds: [],
};

export function FilterView(): JSX.Element {
  const [filterParamsState, filterParamsDispatch] = useReducer(
    filterParamsReducer,
    filtersInitState
  );
  const prevFilterParams = usePrevious(filterParamsState);

  const analyzedFiles = useSelector(selectAnalyzedState);
  const chunksDictionary = useSelector(selectChunksDictionary);
  const prevChunksDictionary = usePrevious(chunksDictionary);

  const dispatch = useDispatch();

  const [, setFilterTimeout] = useState<ReturnType<typeof setTimeout>>(
    setTimeout(() => {}, 0)
  );

  //
  // effects
  //

  useEffect(() => {
    const filter = (initialFilter: boolean) => {
      runFilter(filterParamsState, analyzedFiles, initialFilter, dispatch);
    };

    // must use a compare function caues react doesn't change the pointer
    const filterParamsChange = !_.eq(prevFilterParams, filterParamsState);

    // we need to check whether the chunks have changed, because each time analyzed files change
    // all of the dictionaries get updated.. (ie .bpmdictionary, .keysdictionary etc.)
    const chunksUpdated = prevChunksDictionary !== chunksDictionary;

    // if the filter params change, then set a timeout and refresh filtered results
    if (filterParamsChange) {
      setFilterTimeout((previousTimeout) => {
        clearInterval(previousTimeout);
        return setTimeout(() => {
          console.log(
            'FilterView: Starting: Filtering Files and Reseting Initial Filtered Files..'
          );
          filter(true);
        }, 600);
      });
    } else if (chunksUpdated) {
      console.log(
        'FilterView: Starting: Filtering Files and adding to Added Filtered Files..'
      );
      filter(false);
    }
  }, [
    filterParamsState,
    prevFilterParams,
    analyzedFiles,
    chunksDictionary,
    prevChunksDictionary,
    dispatch,
  ]);

  //
  // event handlers
  //

  function handleRangeChange(val: IBpmRange): void {
    filterParamsDispatch({ type: FPA.SET_BPM_MIN, bpm: val.min });
    filterParamsDispatch({ type: FPA.SET_BPM_MAX, bpm: val.max });
  }

  function handleKeyClick(key: KeyKeys): void {
    filterParamsDispatch({ type: FPA.TOGGLE_KEY, key });
  }

  function handleCommonTagClick(tagId: TagIds): void {
    filterParamsDispatch({ type: FPA.TOGGLE_COMMON_TAGS, tagId });
  }

  //
  // render
  //

  return (
    <div className={styles.filterContainer}>
      <div
        className={classNames({
          [styles.disabledFilter]: !filterParamsState.bpmFilterActive,
        })}
      >
        <label className={styles.filterLabel}>
          <h3>Beats per Minute Filter</h3>
          <Toggle
            defaultChecked={filterParamsState.bpmFilterActive}
            icons={false}
            onChange={() =>
              filterParamsDispatch({
                type: FPA.TOGGLE_BPM_FILTER,
              })
            }
          />
        </label>
        <div className={styles.bpmInputRange}>
          <InputRange
            disabled={!filterParamsState.bpmFilterActive}
            maxValue={BPM_PICKER_MAX}
            minValue={BPM_PICKER_MIN}
            allowSameValues
            value={{
              min: filterParamsState.bpmMin,
              max: filterParamsState.bpmMax,
            }}
            onChange={handleRangeChange}
          />
        </div>
      </div>
      <div
        className={classNames({
          [styles.disabledFilter]: !filterParamsState.bpmFilterActive,
        })}
      >
        <label className={styles.filterLabel}>
          <h3>Keys Filters</h3>
          <Toggle
            defaultChecked={filterParamsState.keysFilterActive}
            icons={false}
            onChange={() =>
              filterParamsDispatch({ type: FPA.TOGGLE_KEYS_FILTER })
            }
          />
        </label>
        <KeyFilterChips
          disabled={!filterParamsState.keysFilterActive}
          onClick={handleKeyClick}
          filterParams={filterParamsState}
        />
      </div>
      <div>
        <label className={styles.filterLabel}>
          <h3>Tag Filters</h3>
        </label>
        <CommonTags
          onClick={handleCommonTagClick}
          filterParams={filterParamsState}
        />
      </div>
    </div>
  );
}
