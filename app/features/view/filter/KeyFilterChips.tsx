import React from 'react';
import Chip from '@material-ui/core/Chip';

import { IFilterParams } from './runFilter';
import { KeyKeys } from '../../../utils/analysers/KeyAnal';
import styles from './KeyFilterChips.css';
import { KeyDictionary } from '../../../utils/KeyDictionary';

interface IKeyUIData {
  visible: string;
  keyKey: KeyKeys;
}

const possibleKeys: IKeyUIData[][] = [
  [
    {
      keyKey: KeyKeys.A_FLAT_MAJ,
      visible: KeyDictionary[KeyKeys.A_FLAT_MAJ].visible,
    },
    {
      keyKey: KeyKeys.A_FLAT_MIN,
      visible: KeyDictionary[KeyKeys.A_FLAT_MIN].visible,
    },
  ],

  [
    {
      keyKey: KeyKeys.A_NAT_MAJ,
      visible: KeyDictionary[KeyKeys.A_NAT_MAJ].visible,
    },
    {
      keyKey: KeyKeys.A_NAT_MIN,
      visible: KeyDictionary[KeyKeys.A_NAT_MIN].visible,
    },
  ],

  [
    {
      keyKey: KeyKeys.B_FLAT_MAJ,
      visible: KeyDictionary[KeyKeys.B_FLAT_MAJ].visible,
    },
    {
      keyKey: KeyKeys.B_FLAT_MIN,
      visible: KeyDictionary[KeyKeys.B_FLAT_MIN].visible,
    },
  ],

  [
    {
      keyKey: KeyKeys.B_NAT_MAJ,
      visible: KeyDictionary[KeyKeys.B_NAT_MAJ].visible,
    },
    {
      keyKey: KeyKeys.B_NAT_MIN,
      visible: KeyDictionary[KeyKeys.B_NAT_MIN].visible,
    },
  ],

  [
    {
      keyKey: KeyKeys.C_NAT_MAJ,
      visible: KeyDictionary[KeyKeys.C_NAT_MAJ].visible,
    },
    {
      keyKey: KeyKeys.C_NAT_MIN,
      visible: KeyDictionary[KeyKeys.C_NAT_MIN].visible,
    },
  ],

  [
    {
      keyKey: KeyKeys.D_FLAT_MAJ,
      visible: KeyDictionary[KeyKeys.D_FLAT_MAJ].visible,
    },
    {
      keyKey: KeyKeys.D_FLAT_MIN,
      visible: KeyDictionary[KeyKeys.D_FLAT_MIN].visible,
    },
  ],

  [
    {
      keyKey: KeyKeys.D_NAT_MAJ,
      visible: KeyDictionary[KeyKeys.D_NAT_MAJ].visible,
    },
    {
      keyKey: KeyKeys.D_NAT_MIN,
      visible: KeyDictionary[KeyKeys.D_NAT_MIN].visible,
    },
  ],

  [
    {
      keyKey: KeyKeys.E_FLAT_MAJ,
      visible: KeyDictionary[KeyKeys.E_FLAT_MAJ].visible,
    },
    {
      keyKey: KeyKeys.E_FLAT_MIN,
      visible: KeyDictionary[KeyKeys.E_FLAT_MIN].visible,
    },
  ],

  [
    {
      keyKey: KeyKeys.E_NAT_MAJ,
      visible: KeyDictionary[KeyKeys.E_NAT_MAJ].visible,
    },
    {
      keyKey: KeyKeys.E_NAT_MIN,
      visible: KeyDictionary[KeyKeys.E_NAT_MIN].visible,
    },
  ],

  [
    {
      keyKey: KeyKeys.F_NAT_MAJ,
      visible: KeyDictionary[KeyKeys.F_NAT_MAJ].visible,
    },
    {
      keyKey: KeyKeys.F_NAT_MIN,
      visible: KeyDictionary[KeyKeys.F_NAT_MIN].visible,
    },
  ],

  [
    {
      keyKey: KeyKeys.G_FLAT_MAJ,
      visible: KeyDictionary[KeyKeys.G_FLAT_MAJ].visible,
    },
    {
      keyKey: KeyKeys.G_FLAT_MIN,
      visible: KeyDictionary[KeyKeys.G_FLAT_MIN].visible,
    },
  ],

  [
    {
      keyKey: KeyKeys.G_NAT_MAJ,
      visible: KeyDictionary[KeyKeys.G_NAT_MAJ].visible,
    },
    {
      keyKey: KeyKeys.G_NAT_MIN,
      visible: KeyDictionary[KeyKeys.G_NAT_MIN].visible,
    },
  ],
];

interface IKeyFilterChipsProps {
  onClick: (key: KeyKeys) => void;
  filterParams: IFilterParams;
  disabled: boolean;
}

export function KeyFilterChips({
  onClick: handleKeyClick,
  filterParams,
  disabled,
}: IKeyFilterChipsProps): JSX.Element {
  function renderChips() {
    return possibleKeys.map((parallelKeys) => {
      return (
        <div className={styles.chipDualContainer} key={parallelKeys[0].keyKey}>
          {parallelKeys.map(({ visible, keyKey }) => {
            const isSelected = filterParams.keys.includes(keyKey);
            return (
              <Chip
                key={keyKey}
                label={visible}
                size="small"
                disabled={disabled}
                variant={isSelected ? 'outlined' : 'default'}
                onClick={() => handleKeyClick(keyKey)}
              />
            );
          })}
        </div>
      );
    });
  }

  return <div>{renderChips()}</div>;
}
