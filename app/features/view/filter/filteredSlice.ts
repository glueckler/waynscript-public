import { createSlice, PayloadAction } from '@reduxjs/toolkit';
// eslint-disable-next-line import/no-cycle
import { RootState } from '../../../store';
import { IFileAnalysis } from '../../../utils/analysers/IFileAnalysis';

const initialState: {
  initialFilteredFiles: IFileAnalysis[];
  addedFilteredFiles: IFileAnalysis[];
} = {
  initialFilteredFiles: [],
  addedFilteredFiles: [],
};

const filteredSlice = createSlice({
  name: 'Filtered Files',
  initialState,
  reducers: {
    replaceInitialFilteredFiles: (
      state,
      action: PayloadAction<IFileAnalysis[]>
    ) => {
      state.initialFilteredFiles = action.payload;
    },
    replaceAddedFilteredFiles: (
      state,
      action: PayloadAction<IFileAnalysis[]>
    ) => {
      state.addedFilteredFiles = action.payload;
    },
    resetFilteredState: (state) => {
      state.initialFilteredFiles = [];
      state.addedFilteredFiles = [];
    },
  },
});

export const {
  replaceInitialFilteredFiles,
  replaceAddedFilteredFiles,
  resetFilteredState,
} = filteredSlice.actions;

export default filteredSlice.reducer;

export const selectAddedFilteredList = (state: RootState): IFileAnalysis[] =>
  state.filteredSlice.addedFilteredFiles;

export const selectInitialFilteredList = (state: RootState): IFileAnalysis[] =>
  state.filteredSlice.initialFilteredFiles;
