/* eslint-disable no-continue */
import { Dispatch } from 'redux';

import { IFileAnalysis } from '../../../utils/analysers/IFileAnalysis';
import {
  IFilterDictionaryState,
  IBpmDictionary,
  IChunksDictionary,
  IKeysDictionary,
} from '../analyze/analyzedSlice';
import {
  replaceInitialFilteredFiles,
  replaceAddedFilteredFiles,
  resetFilteredState,
} from './filteredSlice';
import { KeyKeys } from '../../../utils/analysers/KeyAnal';
import { TagIds } from '../../../utils/analysers/tagDataDictionary';

export interface IFilterParams {
  bpmMin: number;
  bpmMax: number;
  keys: KeyKeys[];
  bpmFilterActive: boolean;
  keysFilterActive: boolean;
  commonTagIds: TagIds[];
}

export const runFilter = (
  {
    bpmMin,
    bpmMax,
    keys: filterParamKeys,
    bpmFilterActive,
    keysFilterActive,
    commonTagIds,
  }: IFilterParams,
  filteredDictionary: IFilterDictionaryState,
  initialFilterProcess: boolean,
  dispatch: Dispatch
) => {
  new Promise<IFileAnalysis[]>((resolve) => {
    let filteredFiles: IFileAnalysis[] = [];

    // this first case covers all case of bpm filters..
    if (bpmFilterActive) {
      filteredFiles = filterBpms(
        filteredDictionary.bpms,
        bpmMin,
        bpmMax,
        commonTagIds
      );

      if (keysFilterActive) {
        filteredFiles = filterKeysOfList(filteredFiles, filterParamKeys);
      }
    } else if (keysFilterActive) {
      // we do not need to filter by bpm in this case..
      filteredFiles = filterKeysOfDictionary(
        filteredDictionary.keys,
        filterParamKeys,
        commonTagIds
      );
    } else if (commonTagIds.length > 0) {
      filteredFiles = allSamplesFromChunks(filteredDictionary.chunks, {
        commonTagIds,
      });
    } else {
      filteredFiles = allSamplesFromChunks(filteredDictionary.chunks, null);
    }

    resolve(filteredFiles);
  })
    .then((filteredFiles) => {
      if (initialFilterProcess) {
        console.log(
          'runFilter: Starting: Filtering Files and Reseting Initial Filtered Files..'
        );
        dispatch(resetFilteredState());
        dispatch(replaceInitialFilteredFiles(filteredFiles));
      } else {
        console.log(
          'runFilter: Finished: Filtering Files and adding to Added Filtered Files..'
        );
        dispatch(replaceAddedFilteredFiles(filteredFiles));
      }
      return filteredFiles;
    })
    .catch((err) => {
      console.error(`runFilter: Error Filtering.... ${err}`);
    });
};

function filterBpms(
  bpmDictionary: IBpmDictionary,
  bpmMin: number,
  bpmMax: number,
  commonTagIds: TagIds[]
): IFileAnalysis[] {
  let filteredFilesBPM: IFileAnalysis[] = [];

  for (let bpm = bpmMin; bpm <= bpmMax; bpm++) {
    if (!bpmDictionary[bpm]) continue;

    if (commonTagIds.length > 0) {
      filteredFilesBPM = filteredFilesBPM.concat(
        bpmDictionary[bpm].filter((anal) => anal.hasTagInList(commonTagIds))
      );
    } else {
      filteredFilesBPM = filteredFilesBPM.concat(bpmDictionary[bpm]);
    }
  }

  // FILTER OUT UNCERTAIN BPMS.. for now..
  const highCertaintyFilteredBPM = filteredFilesBPM.filter(({ bpm }) => {
    if (bpm.result < 3) {
      return false;
    }
    return true;
  });

  return highCertaintyFilteredBPM;
}

function filterKeysOfDictionary(
  keysDictionary: IKeysDictionary,
  filterParamKeys: KeyKeys[],
  commonTagIds: TagIds[]
) {
  let filteredFiles: IFileAnalysis[] = [];

  filterParamKeys.forEach((key) => {
    if (!keysDictionary[key]) return;

    if (commonTagIds.length > 0) {
      filteredFiles = filteredFiles.concat(
        keysDictionary[key].filter((anal) => anal.hasTagInList(commonTagIds))
      );
    } else {
      filteredFiles = filteredFiles.concat(keysDictionary[key]);
    }
  });

  return filteredFiles;
}

function allSamplesFromChunks(
  chunksDictionary: IChunksDictionary,
  variables: { commonTagIds?: TagIds[] } | null
) {
  let files: IFileAnalysis[] = [];

  const commonTagIds = variables?.commonTagIds;
  if (commonTagIds) {
    Object.values(chunksDictionary).forEach((filechunk) => {
      files = files.concat(
        filechunk.filter((anal) => anal.hasTagInList(commonTagIds))
      );
    });
  } else {
    Object.values(chunksDictionary).forEach((filechunk) => {
      files = files.concat(filechunk);
    });
  }

  return files;
}

function filterKeysOfList(faList: IFileAnalysis[], filterParamKeys: KeyKeys[]) {
  return faList.filter(({ hasKey, key: analyzedKey }) => {
    if (!hasKey) return false;

    let keyMatch = false;

    filterParamKeys.forEach((key) => {
      if (key === analyzedKey.key) keyMatch = true;
    });

    return keyMatch;
  });
}
