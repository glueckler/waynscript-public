/* eslint-disable import/no-cycle */
// because we are importing the RootState interface into our slices
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { History } from 'history';
import analyzedSlice from './features/view/analyze/analyzedSlice';
import filteredSlice from './features/view/filter/filteredSlice';
import analyzerStateSlice from './features/view/analyze/analyzerStateSlice';
import filebrowseSlice from './features/view/filebrowse/filebrowseSlice';
import IPCslice from './containers/IPC/IPCslice';
import bpmAnalSlice from './features/logic/bpmAnal/bpmAnalSlice';
import keyAnalSlice from './features/logic/keyAnal/keyAnalSlice';

export default function createRootReducer(history: History) {
  return combineReducers({
    router: connectRouter(history),
    analyzerStateSlice,
    analyzedSlice,
    filteredSlice,
    filebrowse: filebrowseSlice,
    IPC: IPCslice,
    bpmAnal: bpmAnalSlice,
    keyAnal: keyAnalSlice,
  });
}
