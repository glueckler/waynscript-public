import { basename, dirname } from 'path';

export interface IFilePath {
  filename: string;
  directory: string;
  filepath: string;
}

export class FilePath implements IFilePath {
  filepath = '';

  filename = '';

  directory = '';

  constructor(filepath: string) {
    this.filepath = filepath;
    this.filename = basename(filepath);
    this.directory = dirname(filepath);
  }
}
