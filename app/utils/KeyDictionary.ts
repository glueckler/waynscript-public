import { KeyKeys } from './analysers/KeyAnal';

type IKeyDictionary = {
  [Key in `${KeyKeys}`]: { visible: string };
};

export const KeyDictionary: IKeyDictionary = {
  [KeyKeys.NULL_KEY]: { visible: 'No Key' },
  [KeyKeys.A_FLAT_MAJ]: { visible: 'A♭ Maj' },
  [KeyKeys.A_FLAT_MIN]: { visible: 'A♭ min' },

  [KeyKeys.A_NAT_MAJ]: { visible: 'A Maj' },
  [KeyKeys.A_NAT_MIN]: { visible: 'A min' },

  [KeyKeys.B_FLAT_MAJ]: { visible: 'B♭ Maj' },
  [KeyKeys.B_FLAT_MIN]: { visible: 'B♭ min' },

  [KeyKeys.B_NAT_MAJ]: { visible: 'B Maj' },
  [KeyKeys.B_NAT_MIN]: { visible: 'B min' },

  [KeyKeys.C_NAT_MAJ]: { visible: 'C Maj' },
  [KeyKeys.C_NAT_MIN]: { visible: 'C min' },

  [KeyKeys.D_FLAT_MAJ]: { visible: 'D♭ Maj' },
  [KeyKeys.D_FLAT_MIN]: { visible: 'D♭ min' },

  [KeyKeys.D_NAT_MAJ]: { visible: 'D Maj' },
  [KeyKeys.D_NAT_MIN]: { visible: 'D min' },

  [KeyKeys.E_FLAT_MAJ]: { visible: 'E♭ Maj' },
  [KeyKeys.E_FLAT_MIN]: { visible: 'E♭ min' },

  [KeyKeys.E_NAT_MAJ]: { visible: 'E Maj' },
  [KeyKeys.E_NAT_MIN]: { visible: 'E min' },

  [KeyKeys.F_NAT_MAJ]: { visible: 'F Maj' },
  [KeyKeys.F_NAT_MIN]: { visible: 'F min' },

  [KeyKeys.G_FLAT_MAJ]: { visible: 'G♭ Maj' },
  [KeyKeys.G_FLAT_MIN]: { visible: 'G♭ min' },

  [KeyKeys.G_NAT_MAJ]: { visible: 'G Maj' },
  [KeyKeys.G_NAT_MIN]: { visible: 'G min' },
};

export const keyVisible = (key: KeyKeys): string => {
  try {
    return KeyDictionary[key].visible;
  } catch (error) {
    console.warn(`The Key ${key} caused this error on lookup ${error}`);
    return 'key find error';
  }
};
