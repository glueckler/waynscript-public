import _ from 'lodash';
import { TagIds } from './tagDataDictionary';
import { TagDictionary } from './TagAnal';
import { IFileAnalysis } from './IFileAnalysis';
import { DEFAULT_KEY } from './KeyAnal';
import { DEFAULT_BPM } from './BpmAnal';

export abstract class Anal implements IFileAnalysis {
  public hasBpm = false;

  public bpm = DEFAULT_BPM();

  public bpmREMatch = '';

  public isLoop = false;

  public hasKey = false;

  public key = DEFAULT_KEY();

  public keyREMatch = '';

  public hasTag = false;

  public tags: TagDictionary = {};

  public tagIds: TagIds[] = [];

  public filepath = '';

  public directory = '';

  public filename = '';

  public hasTagInList(tagIdsList: TagIds[]): boolean {
    if (!this.hasTag) return false;
    return _.intersection(tagIdsList, this.tagIds).length > 0;
  }
}
