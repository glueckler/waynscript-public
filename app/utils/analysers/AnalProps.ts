import { TagIds } from './tagDataDictionary';
import { TagDictionary } from './TagAnal';
import { IBpm } from './BpmAnal';
import { IKey } from './KeyAnal';

export type AnalProps = {
  hasBpm: boolean;
  bpm: IBpm;
  isLoop: boolean;
  hasKey: boolean;
  key: IKey;
  hasTag: boolean;
  tags: TagDictionary;
  tagIds: TagIds[];
  filepath: string;
  directory: string;
  filename: string;
};
