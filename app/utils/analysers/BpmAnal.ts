import { Result } from '../common.types';

export interface IBpm {
  bpm: number;
  result: Result;
}

export interface IBpmAnal {
  hasBpm: boolean;
  bpm: IBpm;
}

export const DEFAULT_BPM = () => ({ bpm: 0, result: Result.NO_FIND });

export class BpmAnal implements IBpmAnal {
  bpm = DEFAULT_BPM();

  hasBpm = false;

  constructor(stringBeingAnalyzed: string) {
    const testName = stringBeingAnalyzed.replace(/[_-]/g, ' ');

    // check regexs in order of least certain to most

    const notSureMatch = testName.match(
      /(?<!\d)(0?5[1-9]|0?[6-9][0-9]|1[0-9]{2})(?!\d)/i
    );
    if (notSureMatch) {
      this.setBpmAndHasBpm(notSureMatch[1], Result.UNCERTAIN_FIND);
      this.bpm = {
        bpm: parseInt(notSureMatch[1], 10), // note that parse int also helps ex. "060" -> 60
        result: Result.UNCERTAIN_FIND,
      };
    }

    const matchBpmAheadOfNumber = testName.match(
      /bpm[-_\s]?(?<!\d)(0?5[1-9]|0?[6-9][0-9]|1[0-9]{2})(?!\d)/i
    );
    if (matchBpmAheadOfNumber) {
      this.setBpmAndHasBpm(matchBpmAheadOfNumber[1], Result.CERTAIN_FIND);
    }

    const sureMatch = testName.match(
      /(?<!\d)(0?5[1-9]|0?[6-9][0-9]|1[0-9]{2})(?!\d)[-_\s]?bpm/i
    );
    if (sureMatch) {
      this.setBpmAndHasBpm(sureMatch[1], Result.CERTAIN_FIND);
    }
  }

  private setBpmAndHasBpm(bpm: string, result: Result) {
    this.hasBpm = true;
    this.bpm = {
      result,
      bpm: parseInt(bpm, 10), // note that parse int also helps ex. "060" -> 60
    };
  }
}
