import { FilePath } from '../FilePath';
import { FileNameAnalysis } from './FileNameAnalysis';
import { AnalProps } from './AnalProps';
import { IFileAnalysis } from './IFileAnalysis';
import { AnalyserData } from '../../app-support/analyser-data';
import recursiveFileList from '../recursiveFileList';
import { audioExts } from '../../constants/audioExts';
import { AnalysisTypes } from '../../features/view/analyze/AnalysisTypes';

export interface IIpcFilesChunk {
  files?: FilePath[];
  analyzedFiles?: AnalProps[];
  chunkName: string;
  analType: AnalysisTypes;
}

export class DirectoryAnalysis {
  public recursiveFileList: FilePath[] = [];

  constructor(public dirpath: string) {}

  private generateRecursiveFileList(directoryPath: string) {
    this.recursiveFileList = recursiveFileList(
      directoryPath,
      audioExts,
      false
    ).map((filepath) => new FilePath(filepath));
  }

  /**
   * initFileList
   */
  public initFileList() {
    this.generateRecursiveFileList(this.dirpath);
  }

  /**
   * analyzeFiles
   */
  public analyzeFiles(files: FilePath[]): Promise<IFileAnalysis[]> {
    console.log(`DirectoryAnalysis: Analyzing ${files.length} files`);

    const cachedAnalysis = new AnalyserData();

    return new Promise<FileNameAnalysis[]>((resolve) => {
      const analyzedFiles = files.map((file) => {
        // const { directory, filename } = file;

        // // attempt to find in cache
        // const cachedFileAnalysis = cachedAnalysis.getFileAnalysis(
        //   directory,
        //   filename
        // );

        // if (cachedFileAnalysis) {
        //   return new FilePropsAnalysis(cachedFileAnalysis);
        // }

        const fileAnalysis = new FileNameAnalysis(file);

        // cachedAnalysis.setFileAnalysis(directory, filename, fileAnalysis);

        return fileAnalysis;
      });

      // Write cache to storage..
      cachedAnalysis.save();

      resolve(analyzedFiles);
    });
  }
}
