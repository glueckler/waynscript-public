import fs from 'fs';

import { Anal } from './Anal.abstract';
import { FilePath } from '../FilePath';
import { BpmAnal } from './BpmAnal';
import { KeyAnal } from './KeyAnal';
import { LoopAnal } from './LoopAnal';
import { isLoopLength } from './isLoopLength';
import { TagAnal } from './TagAnal';

export class FileNameAnalysis extends Anal {
  constructor(file: FilePath) {
    super();
    this.filepath = file.filepath;
    this.directory = file.directory;
    this.filename = file.filename;

    const bpm = new BpmAnal(file.filename);
    this.hasBpm = bpm.hasBpm;
    this.bpm = bpm.bpm;

    const key = new KeyAnal(file.filename);
    this.hasKey = key.hasKey;
    this.key = key.key;

    const loop = new LoopAnal(file.filename);
    this.isLoop = loop.isLoop;

    const tags = new TagAnal(file.filename);
    this.hasTag = tags.hasTag;
    this.tags = tags.tags;
    this.tagIds = tags.tagIds;

    // if (this.isLoop || this.hasBpm) {
    //   // then check the length..
    //   if (fs.lstatSync(file.filepath).isFile()) {
    //     const isLongEnough = isLoopLength(file.filepath);
    //     if (!isLongEnough) {
    //       this.isLoop = false;
    //       this.hasBpm = false;
    //     }
    //   }
    // }
  }
}
