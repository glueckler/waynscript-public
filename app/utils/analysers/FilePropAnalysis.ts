import { AnalProps } from './AnalProps';
import { Anal } from './Anal.abstract';

export class FilePropsAnalysis extends Anal {
  constructor(properties: AnalProps) {
    super();
    this.hasBpm = properties.hasBpm;
    this.bpm = properties.bpm;
    this.isLoop = properties.isLoop;
    this.hasKey = properties.hasKey;
    this.key = properties.key;
    this.hasTag = properties.hasTag;
    this.tags = properties.tags;
    this.tagIds = properties.tagIds;
    this.filepath = properties.filepath;
    this.directory = properties.directory;
    this.filename = properties.filename;
  }
}
