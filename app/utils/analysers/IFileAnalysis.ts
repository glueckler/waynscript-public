import { IFilePath } from '../FilePath';
import { IBpmAnal } from './BpmAnal';
import { IKeyAnal } from './KeyAnal';
import { ILoopAnal } from './LoopAnal';
import { ITagAnal } from './TagAnal';
import { TagIds } from './tagDataDictionary';

export interface IFileAnalysis
  extends IFilePath,
    ITagAnal,
    IBpmAnal,
    IKeyAnal,
    ILoopAnal {
  hasTagInList: (list: TagIds[]) => boolean;
}
