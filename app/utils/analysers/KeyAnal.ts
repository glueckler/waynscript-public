import { Result } from '../common.types';

export enum KeyKeys {
  NULL_KEY = 'NULL_KEY',
  A_FLAT_MAJ = 'A_FLAT_MAJ',
  A_FLAT_MIN = 'A_FLAT_MIN',
  A_NAT_MAJ = 'A_NAT_MAJ',
  A_NAT_MIN = 'A_NAT_MIN',

  B_FLAT_MAJ = 'B_FLAT_MAJ',
  B_FLAT_MIN = 'B_FLAT_MIN',
  B_NAT_MAJ = 'B_NAT_MAJ',
  B_NAT_MIN = 'B_NAT_MIN',

  C_NAT_MAJ = 'C_NAT_MAJ',
  C_NAT_MIN = 'C_NAT_MIN',

  D_FLAT_MAJ = 'D_FLAT_MAJ',
  D_FLAT_MIN = 'D_FLAT_MIN',
  D_NAT_MAJ = 'D_NAT_MAJ',
  D_NAT_MIN = 'D_NAT_MIN',

  E_FLAT_MAJ = 'E_FLAT_MAJ',
  E_FLAT_MIN = 'E_FLAT_MIN',
  E_NAT_MAJ = 'E_NAT_MAJ',
  E_NAT_MIN = 'E_NAT_MIN',

  F_NAT_MAJ = 'F_NAT_MAJ',
  F_NAT_MIN = 'F_NAT_MIN',

  G_FLAT_MAJ = 'G_FLAT_MAJ',
  G_FLAT_MIN = 'G_FLAT_MIN',
  G_NAT_MAJ = 'G_NAT_MAJ',
  G_NAT_MIN = 'G_NAT_MIN',
}

export interface IKey {
  key: KeyKeys;
  result: Result;
}

export interface IKeyAnal {
  hasKey: boolean;
  key: IKey;
}

// this object must be cloned not referenced!
export const DEFAULT_KEY = () => ({
  key: KeyKeys.NULL_KEY,
  result: Result.NO_FIND,
});

export class KeyAnal implements IKeyAnal {
  public hasKey = false;

  public key = DEFAULT_KEY();

  private pitchLetter = '';

  private minor = false;

  private sharp = false;

  private flat = false;

  constructor(filename: string) {
    const testName = filename.replace(/[_-]/g, ' ');

    // PRETTY_SURE Certainty tests
    const prettySureMajor = testName.match(/\b([A-G])\b/i);
    if (prettySureMajor) {
      this.sharp = false;
      this.flat = false;
      this.minor = false;
      this.pitchLetter = prettySureMajor[1].toUpperCase();
      this.assignKey();
      this.key.result = Result.CERTAIN_FIND;
    }

    // SURE Certainty tests
    const sureMatchSharp = testName.match(
      /\b([A-G])(\#|(?:\s?sharp))(?:maj(?:or)?)?\s?(m(?:in)?(?:or)?)?\b/i
    );
    if (sureMatchSharp) {
      this.sharp = true;
      this.flat = false;
      if (sureMatchSharp[3]) this.minor = true;
      this.pitchLetter = sureMatchSharp[1].toUpperCase();
      this.assignKey();
      this.key.result = Result.CERTAIN_FIND;
      return;
    }

    const sureMatchFlat = testName.match(
      /\b([A-G])(b|(?:\s?flat))(?:maj(?:or)?)?\s?(m(?:in)?(?:or)?)?\b/i
    );
    if (sureMatchFlat) {
      if (sureMatchFlat[0].includes('CB')) return;
      this.sharp = false;
      this.flat = true;
      if (sureMatchFlat[3]) this.minor = true;
      this.pitchLetter = sureMatchFlat[1].toUpperCase();
      this.assignKey();
      this.key.result = Result.CERTAIN_FIND;
      return;
    }

    const sureMatchMin = testName.match(/\b([A-G])\s?(?:m(?:in)?(?:or)?)\b/i);
    if (sureMatchMin) {
      this.sharp = false;
      this.flat = false;
      this.minor = true;
      this.pitchLetter = sureMatchMin[1].toUpperCase();
      this.assignKey();
      this.key.result = Result.CERTAIN_FIND;
      return;
    }

    const sureMajor = testName.match(/\b([A-G])\s?maj(?:or)?\b/i);
    if (sureMajor) {
      this.sharp = false;
      this.flat = false;
      this.minor = false;
      this.pitchLetter = sureMajor[1].toUpperCase();
      this.assignKey();
      this.key.result = Result.CERTAIN_FIND;
      return;
    }
  }

  private swapEnharmonic() {
    if (['C', 'D', 'F', 'G', 'A'].includes(this.pitchLetter)) {
      this.flat = true;
      this.sharp = false;
    }
    if (this.pitchLetter === 'C') {
      this.pitchLetter = 'D';
    } else if (this.pitchLetter === 'D') {
      this.pitchLetter = 'E';
    } else if (this.pitchLetter === 'F') {
      this.pitchLetter = 'G';
    } else if (this.pitchLetter === 'G') {
      this.pitchLetter = 'A';
    } else if (this.pitchLetter === 'A') {
      this.pitchLetter = 'B';
    }
  }

  private assignKey() {
    if (this.sharp) this.swapEnharmonic();

    const keyKeyDynamic = `${this.pitchLetter}_${this.flat ? 'FLAT' : 'NAT'}_${
      this.minor ? 'MIN' : 'MAJ'
    }`;

    this.key.key = KeyKeys[keyKeyDynamic as KeyKeys]; // this is a lil hack since we shouldnt be using a dynamic string to search from enum
    // there are some edge cases with this though.. I noticed C_FLAT_MAJ come through..
    // so just in case
    if (!this.key.key) this.key.key = KeyKeys.NULL_KEY;

    this.hasKey = true;
  }
}
