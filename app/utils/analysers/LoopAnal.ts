export interface ILoopAnal {
  isLoop: boolean;
}

export class LoopAnal implements ILoopAnal {
  public isLoop = false;

  constructor(stringBeingAnalyzed: string) {
    const loopMatches = stringBeingAnalyzed.match(/loop(?:s)?(?:[-_\s]|$)/i);
    if (!loopMatches) return;
    this.isLoop = true;
  }
}
