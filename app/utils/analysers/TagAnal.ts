import { Result } from '../common.types';
import { tagDataDictionary, TagIds } from './tagDataDictionary';

export interface ITag {
  tagId: TagIds;
  result: Result;
}

export type TagDictionary = {
  [Key in `${TagIds}`]?: ITag;
};

export interface ITagAnal {
  hasTag: boolean;
  tags: TagDictionary;
  tagIds: TagIds[];
}

export class TagAnal implements ITagAnal {
  public hasTag = false;

  public tags: TagDictionary = {};

  public tagIds: TagIds[] = [];

  constructor(name: string) {
    Object.values(TagIds).forEach((tag) => {
      const tagData = tagDataDictionary[tag];

      if (tagData === undefined) return;

      const { tagId, regexs } = tagData;

      const determineMatch = (result: Result): boolean => {
        const tests = regexs[result];
        if (!tests) return false;

        let match = false;
        tests.forEach((re) => {
          if (match) return;

          if (re.test(name.replace(/[_-]/g, ' '))) {
            this.tags[tagId] = {
              result,
              tagId,
            };
            match = true;
            if (result !== Result.NO_FIND) {
              this.hasTag = true;
              this.tagIds.push(tagId);
            }
          }
        });
        return match;
      };

      if (regexs[Result.NO_FIND]) {
        const match = determineMatch(Result.NO_FIND);
        if (match) return;
      }
      if (regexs[Result.CERTAIN_FIND]) {
        const match = determineMatch(Result.CERTAIN_FIND);
        if (match) return;
      }
      if (regexs[Result.CERTAIN_FIND]) {
        const match = determineMatch(Result.CERTAIN_FIND);
        if (match) return;
      }
      if (regexs[Result.UNCERTAIN_FIND]) {
        determineMatch(Result.UNCERTAIN_FIND);
      }
    });
  }

  // this function was used in development to help find what the regexs were missing
  // by performing a very broad search and comparing the regexs to it
  // then using your eyes to see if there's anything missing
  static developmentMassSearch(
    stringList: string[],
    tagId: TagIds,
    massSearch: string
  ) {
    stringList.forEach((name) => {
      const tagAnal = new TagAnal(name);
      if (tagAnal.tagIds.includes(tagId)) return;
      console.log(
        `\nThe Following string is not mathing the mass search '${massSearch}' for Tag: ${tagId}`
      );
      console.log(`string: ${name}`);
    });
  }
}
