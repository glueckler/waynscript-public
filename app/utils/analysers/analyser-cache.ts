import { IFileAnalysis } from './IFileAnalysis';

// first key is the directory, second is the filename
export interface IAnalyserStoredData {
  [key: string]: { [key: string]: IFileAnalysis };
}

export const ANALYSER_CACHE: IAnalyserStoredData = {};
