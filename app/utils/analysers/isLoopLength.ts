import { getAudioDurationInSeconds } from '../ffprobe/get-audio-duration';

const LOOP_MIN_LENGTH = 2;

export function isLoopLength(filepath: string): boolean {
  let seconds;
  try {
    seconds = getAudioDurationInSeconds(filepath);
    return Math.floor(seconds) > LOOP_MIN_LENGTH;
  } catch (err) {
    console.error(`finding the loop length of ${filepath}`);
    console.error(err);
    return err;
  }
}
