import { oneshots } from './oneshots-list';

function reGen() {
  // build a regex that looks like this /(kicks)|(snares)|(etc)/
  return oneshots
    .map(([re]) => new RegExp(`(${re.source})`))
    .reduce((reCollection, re) => {
      return new RegExp(`${reCollection.source}|${re.source}`);
    });
}

function whichOneShot(match: RegExpExecArray) {
  for (let i = 1; i < oneshots.length + 1; i += 1) {
    if (match[i] !== undefined) return oneshots[i - 1][1];
  }
  return '';
}

export class Oneshot {
  private isOneshotPriv = false;

  private oneshotTypePriv = '';

  public oneshotREMatch = '';

  constructor(filename: string) {
    const match = reGen().exec(filename);
    if (!match) return;

    [this.oneshotREMatch] = match;
    this.isOneshotPriv = true;
    this.oneshotTypePriv = whichOneShot(match);
  }

  /**
   * isOneShot
   */
  public isOneshot() {
    return this.isOneshotPriv;
  }

  /**
   * oneShotType
   */
  public oneshotType() {
    return this.oneshotTypePriv;
  }
}
