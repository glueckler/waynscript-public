type OneshotDefinition = [RegExp, string];

export const oneshots: Array<OneshotDefinition> = [
  [/kick(?:s)?(?:drums)?/, 'kicks'],
  [/snare(?:s)?/, 'snares'],
  [/hihat(?:s)?/, 'hihats'],
];
