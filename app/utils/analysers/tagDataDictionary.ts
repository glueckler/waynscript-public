import { Result } from '../common.types';

export enum TagIds {
  CLAP = 'CLAP',
  NULLTAG = 'NULLTAG',
  KICK = 'KICK',
  SNARE = 'SNARE',
  ROLL = 'SNARE_ROLL',
  HIHAT = 'HIHAT',
  CLOSED_HIHAT = 'CLOSED_HIHAT',
  OPEN_HIHAT = 'OPEN_HIHAT',
  CRASH = 'CRASH',
  TOM = 'TOM',
  CLAVE = 'CLAVE',
  PERCUSSION = 'PERCUSSION',
  BREAK = 'BREAK',
  RIDE = 'RIDE',
  SHAKER = 'SHAKER',
  EIGHTOHEIGHT = 'EIGHTOHEIGHT',
  CONGA = 'CONGA',
  RIM = 'RIM',
  BONGO = 'BONGO',
  TAM = 'TAM',
  SNAP = 'SNAP',
  FILL = 'FILL',
  BELL = 'BELL',
  CAJON = 'CAJON',
}

export interface ITagAnalysisData {
  tagId: TagIds;
  regexs: {
    [P in `${Result}`]?: RegExp[];
  };
  text: string;
  development?: { [key: string]: any };
}

// found this little diddy here (typescript litereals)
// https://github.com/microsoft/TypeScript/issues/22892#issuecomment-772302314
export type ITagDataDictionary = {
  [P in `${TagIds}`]?: ITagAnalysisData;
};

export const tagDataDictionary: ITagDataDictionary = {
  [TagIds.CLAP]: {
    tagId: TagIds.CLAP,
    text: 'clap',
    regexs: {
      [Result.CERTAIN_FIND]: [/\bcla?p[zs]?\d*\b/i],
      [Result.CERTAIN_FIND]: [/\bcla?p/i, /cla?p[zs]?\d*\b/i],
      [Result.UNCERTAIN_FIND]: [/cla?p/i],
    },
    development: { massSearch: 'clap', development: false },
  },
  [TagIds.KICK]: {
    tagId: TagIds.KICK,
    text: 'kick',
    regexs: {
      [Result.NO_FIND]: [/\bno(\s)?kick\d*\b/i],
      [Result.UNCERTAIN_FIND]: [/\bbassdrum[zs]?\d*\b/i],
      [Result.CERTAIN_FIND]: [/kick(?!in)[zs]?\d*/i],
      [Result.CERTAIN_FIND]: [/\bkick[zs]?\d*\b/i],
    },
    development: { massSearch: 'kick', development: false },
  },
  [TagIds.SNARE]: {
    tagId: TagIds.SNARE,
    text: 'snare',
    regexs: {
      [Result.NO_FIND]: [/snare(\s)?roll/i],
      [Result.CERTAIN_FIND]: [/snare[zs]?/i],
      [Result.CERTAIN_FIND]: [/\bsnare[zs]?\d*\b/i],
    },
    development: { massSearch: 'snare', development: false },
  },
  [TagIds.ROLL]: {
    tagId: TagIds.ROLL,
    text: 'snare roll',
    regexs: {
      [Result.UNCERTAIN_FIND]: [/roll/i],
      [Result.CERTAIN_FIND]: [/\broll/i, /roll[zs]?\d*\b/i],
      [Result.CERTAIN_FIND]: [/\broll[zs]?\d*\b/i],
    },
    development: { massSearch: 'roll', development: false },
  },
  [TagIds.HIHAT]: {
    tagId: TagIds.HIHAT,
    text: 'hihat',
    regexs: {
      [Result.UNCERTAIN_FIND]: [/\bhhloop/i, /\s?hh[co]?\d*\b/i],
      [Result.CERTAIN_FIND]: [
        /(hi\s?)?hat[zs]?\d*/i,
        /\bhh\b/i,
        /hat[zs]?\d*\b/i,
      ],
      [Result.CERTAIN_FIND]: [/\b(hi\s?)hat[zs]?\d*\b/i],
    },
    development: { massSearch: 'hh', development: false },
  },
  [TagIds.CLOSED_HIHAT]: {
    tagId: TagIds.CLOSED_HIHAT,
    text: 'closed hat',
    regexs: {
      [Result.UNCERTAIN_FIND]: [/hatloopclosed/i, /\s?hhc\d*\b/i],
      [Result.CERTAIN_FIND]: [
        /closed\s?(((hi)?\s?hat)[zs]?|(hh\b))/i,
        /((\bhh)|((hi)?\s?hat))[zs]?\s?closed/i,
      ],
      [Result.CERTAIN_FIND]: [
        /\bclosed\s?(((hi)?\s?hat)|(hh\b))[zs]?\b/i,
        /\b((\bhh)|((hi)?\s?hat))[zs]?\s?closed\b/i,
        /(hi\s?)hat[zs]?\d*\(closed\)/i,
      ],
    },
    development: { massSearch: 'closed', development: false },
  },
  [TagIds.OPEN_HIHAT]: {
    tagId: TagIds.OPEN_HIHAT,
    text: 'open hihat',
    regexs: {
      [Result.UNCERTAIN_FIND]: [/hatloopopen/i, /\s?hho\d*\b/i],
      [Result.CERTAIN_FIND]: [
        /open\s?(((hi)?\s?hat)[zs]?|(hh\b))/i,
        /((\bhh)|((hi)?\s?hat))[zs]?\s?open/i,
      ],
      [Result.CERTAIN_FIND]: [
        /\bopen\s?(((hi)?\s?hat)|(hh\b))[zs]?\b/i,
        /\b((\bhh)|((hi)?\s?hat))[zs]?\s?open\b/i,
        /(hi\s?)hat[zs]?\d*\(open\)/i,
      ],
    },
    development: { massSearch: 'open', development: false },
  },
  [TagIds.CRASH]: {
    tagId: TagIds.CRASH,
    text: 'crash',
    regexs: {
      [Result.CERTAIN_FIND]: [/\bcrash[zs]?\d*\b/i],
    },
    development: { massSearch: 'crash', development: false },
  },
  [TagIds.TOM]: {
    tagId: TagIds.TOM,
    text: 'tom',
    regexs: {
      [Result.CERTAIN_FIND]: [/\btom[zs]?\d*\b/i],
    },
    development: { massSearch: 'tom', development: false },
  },
  [TagIds.CLAVE]: {
    tagId: TagIds.CLAVE,
    text: 'clave',
    regexs: {
      [Result.CERTAIN_FIND]: [/\bclave[zs]?\d*\b/i],
    },
    development: { massSearch: 'clave', development: false },
  },
  [TagIds.PERCUSSION]: {
    tagId: TagIds.PERCUSSION,
    text: 'perc',
    regexs: {
      [Result.CERTAIN_FIND]: [/\bperc(ussion)?[zs]?\d*\b/i],
      [Result.CERTAIN_FIND]: [/\bperc(ussion)?[zs]?\d*/i],
    },
  },
  [TagIds.BREAK]: {
    tagId: TagIds.BREAK,
    text: 'break',
    regexs: {
      [Result.CERTAIN_FIND]: [/\bbreak[zs]?\d*\b/i],
      [Result.CERTAIN_FIND]: [/\bbreak[zs]?\d*/i],
    },
  },
  [TagIds.RIDE]: {
    tagId: TagIds.RIDE,
    text: 'ride',
    regexs: {
      [Result.CERTAIN_FIND]: [/\bride[zs]?\d*\b/i],
    },
  },
  [TagIds.SHAKER]: {
    tagId: TagIds.SHAKER,
    text: 'shaker',
    regexs: {
      [Result.CERTAIN_FIND]: [/\bshaker[zs]?\d*\b/i],
    },
  },
  [TagIds.EIGHTOHEIGHT]: {
    tagId: TagIds.EIGHTOHEIGHT,
    text: '808',
    regexs: {
      [Result.CERTAIN_FIND]: [/\b808[zs]?\d*\b/i],
    },
  },
  [TagIds.CONGA]: {
    tagId: TagIds.CONGA,
    text: 'conga',
    regexs: {
      [Result.CERTAIN_FIND]: [/\bconga[zs]?\d*\b/i],
    },
  },
  [TagIds.RIM]: {
    tagId: TagIds.RIM,
    text: 'rim',
    regexs: {
      [Result.CERTAIN_FIND]: [/\brim(shot)?[zs]?\d*\b/i],
    },
  },
  [TagIds.BONGO]: {
    tagId: TagIds.BONGO,
    text: 'bongo',
    regexs: {
      [Result.CERTAIN_FIND]: [/\bbongo[zs]?\d*\b/i],
    },
  },
  [TagIds.TAM]: {
    tagId: TagIds.TAM,
    text: 'tam',
    regexs: {
      [Result.CERTAIN_FIND]: [/\btam(b)?(ourine)?[zs]?\d*\b/i],
    },
  },
  [TagIds.SNAP]: {
    tagId: TagIds.SNAP,
    text: 'snap',
    regexs: {
      [Result.CERTAIN_FIND]: [/\bsnap[zs]?\d*\b/i],
    },
  },
  [TagIds.FILL]: {
    tagId: TagIds.FILL,
    text: 'fill',
    regexs: {
      [Result.CERTAIN_FIND]: [/\bfill[zs]?\d*\b/i],
    },
  },
  [TagIds.BELL]: {
    tagId: TagIds.BELL,
    text: 'bell',
    regexs: {
      [Result.CERTAIN_FIND]: [/\bbell[zs]?\d*\b/i],
      [Result.CERTAIN_FIND]: [/bell/i],
    },
  },
  [TagIds.CAJON]: {
    tagId: TagIds.CAJON,
    text: 'cajon',
    regexs: {
      [Result.CERTAIN_FIND]: [/\bcajon[zs]?\d*\b/i],
    },
  },
};
