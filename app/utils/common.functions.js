const transform = (obj, predicate) => {
  return Object.keys(obj).reduce((memo, key) => {
    if (predicate(obj[key], key)) {
      memo[key] = obj[key];
    }
    return memo;
  }, {});
};

// lodash will drop support for omit in the future I believe
// see stackoverflow https://stackoverflow.com/a/40339196
export const omit = (obj, items) =>
  transform(obj, (value, key) => !items.includes(key));

export const pick = (obj, items) =>
  transform(obj, (value, key) => items.includes(key));
