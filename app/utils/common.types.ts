export type Filelist = string[];

export interface IFileMap {
  [key: string]: { path: string };
}

export enum Result {
  NOT_YET_ANAL = 'NOT_ANAL',
  NO_FIND = 'NO_FIND',
  UNCERTAIN_FIND = 'UNCERTAIN_FIND',
  CERTAIN_FIND = 'CERTAIN_FIND',
}
