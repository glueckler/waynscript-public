export const escapeShell = (cmd: string) =>
  `${cmd.replace(/(["\s'$`\\])/g, '\\$1')}`;
