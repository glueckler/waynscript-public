import execa from 'execa';
import { ffprobe } from '../../binaries';
import { escapeShell } from '../escape-shell';

function getFFprobeWrappedExecution(input: string): execa.ExecaSyncReturnValue {
  const params = [
    '-v',
    'error',
    '-select_streams',
    'a:0',
    '-show_format',
    '-show_streams',
  ];

  if (typeof input === 'string') {
    return execa.commandSync(
      `${ffprobe} ${params.join(' ')} ${escapeShell(input)}`
    );
  }

  throw new Error('Given input was not a string');
}

/**
 * Returns a promise that will be resolved with the duration of given audio in
 * seconds.
 *
 * @param  {string|ReadableStream} input Stream or path to file to be used as
 * input for `ffprobe`.
 *
 * @return {Promise} Promise that will be resolved with given audio duration in
 * seconds.
 */
export function getAudioDurationInSeconds(input: string): number {
  const { stdout } = getFFprobeWrappedExecution(input);
  const matched = stdout.match(/duration="?(\d*\.\d*)"?/);
  if (matched && matched[1]) return parseFloat(matched[1]);
  throw new Error('No duration found!');
}
