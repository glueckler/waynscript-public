import fs from 'fs';
import path from 'path';

export type CopyableFileList = [string, string][];

export function fileCopy(
  sortedFiles: CopyableFileList,
  dryrun: boolean | undefined
): void {
  sortedFiles.forEach(([src, dest]) => {
    // copy the files
    if (dryrun) {
      // eslint-disable-next-line no-console
      console.log(`\n${src}\n${dest}\n`);
    } else {
      const sortedFileMasterDir = path.dirname(dest);

      // create dir if doesn't exist
      if (!fs.existsSync(sortedFileMasterDir)) {
        fs.mkdirSync(sortedFileMasterDir, { recursive: true });
      }
      fs.copyFileSync(src, dest);
      // eslint-disable-next-line no-console
      console.log(`\nCOPIED!:\n${src}\n${dest}\n`);
    }
  });
}
