import { BrowserWindow } from 'electron';

export const MSG_FROM_RENDERER_TO_WORKER = 'MSG_FROM_RENDERER_TO_WORKER';
export const MSG_FROM_WORKER_TO_RENDERER = 'MSG_FROM_WORKER_TO_RENDERER';

export function sendWindowMessage(
  targetWindow: BrowserWindow | null,
  message: string,
  payload: string
) {
  // eslint-disable-next-line eqeqeq
  if (targetWindow == undefined) {
    console.log('IPC: Main: Target window does not exist');
    return;
  }
  targetWindow.webContents.send(message, payload);
}
