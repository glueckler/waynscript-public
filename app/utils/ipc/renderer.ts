import { MSG_FROM_RENDERER_TO_WORKER } from './main';

const electron = require('electron');

const { ipcRenderer } = electron;

export const messageWorker = (messageFromRenderer: string, payload: string) => {
  ipcRenderer.send(MSG_FROM_RENDERER_TO_WORKER, {
    message: messageFromRenderer,
    payload,
  });
};
