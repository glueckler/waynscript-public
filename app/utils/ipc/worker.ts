import { MSG_FROM_WORKER_TO_RENDERER } from './main';

const electron = require('electron');

const { ipcRenderer } = electron;

export const messageRenderer = (messageFromWorker: string, payload: string) => {
  ipcRenderer.send(MSG_FROM_WORKER_TO_RENDERER, {
    message: messageFromWorker,
    payload,
  });
};
