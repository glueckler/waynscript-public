import fs from 'fs';
import _ from 'lodash';
import path from 'path';
import { audioExts } from '../constants/audioExts';
import { IFileMap } from './common.types';

const recursiveFileList = (
  directoryPath: string,
  includeExts: string[],
  includeAnyExt: boolean
): string[] => {
  let filelist: string[] = [];
  fs.readdirSync(directoryPath, { withFileTypes: true }).forEach((file) => {
    if (file.isDirectory()) {
      const recursivePath = path.resolve(directoryPath, file.name);
      filelist = filelist.concat(
        recursiveFileList(recursivePath, includeExts, includeAnyExt)
      );
    }
    const ext = path.extname(file.name);
    if (!includeAnyExt && !includeExts.includes(ext)) {
      return;
    }
    filelist.push(path.resolve(directoryPath, file.name));
  });
  return filelist;
};

export const allAudioFilesInDir = (dir: string): string[] => {
  return recursiveFileList(dir, audioExts, false);
};

export const audioFileMapOfDir = (dir: string): IFileMap => {
  const arrayOfFiles = allAudioFilesInDir(dir).map((filename) => {
    return { path: filename };
  });
  return _.keyBy(arrayOfFiles, 'path');
};
