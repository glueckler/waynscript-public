import path from 'path';
import { orderBy, last } from 'lodash';
import { IFileAnalysis } from '../analysers/IFileAnalysis';
import { CopyableFileList } from '../filecopy';
import { ISorter } from './ISorter';

export class BpmSorter implements ISorter {
  public includeLoops = false;

  public includeNonLoops = false;

  constructor(
    public masterDestinationPath: string,
    opts: { includeLoops: boolean; includeNonLoops: boolean }
  ) {
    this.includeLoops = opts.includeLoops;
    this.includeNonLoops = opts.includeNonLoops;
  }

  /**
   * sort
   */
  public sort(analysedFiles: IFileAnalysis[]): CopyableFileList {
    const filteredAnalysedFiles = analysedFiles
      .filter(({ hasBpm }) => hasBpm)
      .filter(({ isLoop }) => {
        if (this.includeLoops && isLoop) return true;
        if (this.includeNonLoops && !isLoop) return true;
        return false;
      });
    const allBpmDict = this.seperateEachBpm(filteredAnalysedFiles);
    const seperatedBpmDict = this.seperateIntoRanges(allBpmDict);
    return this.generateSortedFiles(seperatedBpmDict);
  }

  private seperateEachBpm(
    analysedFiles: IFileAnalysis[]
  ): { [key: number]: IFileAnalysis[] } {
    const bpmDict: { [key: number]: IFileAnalysis[] } = {};
    analysedFiles.forEach((analysedFile) => {
      const { bpm } = analysedFile;
      let bpmNum = bpm.bpm;
      if (bpmNum < 95) bpmNum *= 2;
      if (!bpmDict[bpmNum]) bpmDict[bpmNum] = [];
      bpmDict[bpmNum].push(analysedFile);
    });
    return bpmDict;
  }

  private seperateIntoRanges(allBpmDict: {
    [key: number]: IFileAnalysis[];
  }): [number, number, IFileAnalysis[]][] {
    const MAX_FILES = 100;
    const MAX_BPM_RANGE = 5;
    // first build something like this.. (ok to have the same low and high bpm)
    // [lowbpm, highbpm, [list, of, files...]]
    // that should be enough organization..
    const seperatedList: [number, number, IFileAnalysis[]][] = [];

    const bpmKeys = Object.keys(allBpmDict).map((str) => parseInt(str, 10));
    const bpmKeysSorted = orderBy(bpmKeys);

    bpmKeysSorted.forEach((bpm) => {
      let currentOpenRange = last(seperatedList);
      // create new one if..
      // otherwise add the samples in there
      if (
        !currentOpenRange ||
        currentOpenRange[2].length > MAX_FILES ||
        currentOpenRange[0] <= bpm - MAX_BPM_RANGE
      ) {
        currentOpenRange = [bpm, bpm, [...allBpmDict[bpm]]];
        seperatedList.push(currentOpenRange);
        return;
      }
      currentOpenRange[2] = [...currentOpenRange[2], ...allBpmDict[bpm]];
      currentOpenRange[1] = bpm;
    });

    return seperatedList;
  }

  private generateSortedFiles(
    seperatedBpmDict: [number, number, IFileAnalysis[]][]
  ): CopyableFileList {
    let sortedFiles: CopyableFileList = [];
    seperatedBpmDict.forEach(([lowBpm, highBpm, fileAnals]) => {
      sortedFiles = [
        ...sortedFiles,
        ...fileAnals.map(
          ({ filepath, filename }) =>
            [
              filepath,
              this.generateSortedFilepath(filename, lowBpm, highBpm),
            ] as [string, string]
        ),
      ];
    });
    return sortedFiles;
  }

  private generateSortedFilepath(
    filename: string,
    lowBpm: number,
    highBpm: number
  ): string {
    const bpmRange = lowBpm === highBpm ? `${lowBpm}` : `${lowBpm}-${highBpm}`;
    return path.join(this.masterDestinationPath, 'bpm', bpmRange, filename);
  }
}
