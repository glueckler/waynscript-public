import { IFileAnalysis } from '../analysers/IFileAnalysis';
import { CopyableFileList } from '../filecopy';

export interface ISorter {
  sort(analList: IFileAnalysis[]): CopyableFileList;
}
