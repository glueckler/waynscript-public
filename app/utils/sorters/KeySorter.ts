import path from 'path';
import { IFileAnalysis } from '../analysers/IFileAnalysis';
import { ISorter } from './ISorter';
import { CopyableFileList } from '../filecopy';

export class KeySorter implements ISorter {
  public includeLoops = false;

  public includeNonLoops = false;

  constructor(
    public masterDestinationPath: string,
    opts: { includeLoops: boolean; includeNonLoops: boolean }
  ) {
    this.includeLoops = opts.includeLoops;
    this.includeNonLoops = opts.includeNonLoops;
  }

  /**
   * sort
   */
  public sort(analysedFiles: IFileAnalysis[]): CopyableFileList {
    const keyDict = this.seperateEachKey(
      analysedFiles
        .filter(({ hasKey }) => hasKey)
        .filter(({ isLoop }) => {
          if (this.includeLoops && isLoop) return true;
          if (this.includeNonLoops && !isLoop) return true;
          return false;
        })
    );
    return this.generateSortedFiles(keyDict);
  }

  private seperateEachKey(
    analysedFiles: IFileAnalysis[]
  ): { [key: string]: IFileAnalysis[] } {
    const KeyDict: { [key: string]: IFileAnalysis[] } = {};
    analysedFiles.forEach((analysedFile) => {
      const { key } = analysedFile;
      if (!KeyDict[key.key]) KeyDict[key.key] = [];
      KeyDict[key.key].push(analysedFile);
    });
    return KeyDict;
  }

  private generateSortedFiles(keysDict: {
    [key: string]: IFileAnalysis[];
  }): CopyableFileList {
    const sortedFiles: CopyableFileList = [];
    Object.keys(keysDict).forEach((key) => {
      keysDict[key].forEach(({ filename, filepath }) => {
        sortedFiles.push([
          filepath,
          this.generateSortedFilepath(filename, key),
        ]);
      });
    });
    return sortedFiles;
  }

  private generateSortedFilepath(filename: string, key: string): string {
    return path.join(this.masterDestinationPath, 'key', key, filename);
  }
}
