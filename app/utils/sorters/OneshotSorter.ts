import path from 'path';
import { ISorter } from './ISorter';
import { IFileAnalysis } from '../analysers/IFileAnalysis';
import { CopyableFileList } from '../filecopy';

export class OneshotSorter implements ISorter {
  constructor(public masterDestinationPath: string) {}

  /**
   * sort
   */
  public sort(analysedFiles: IFileAnalysis[]): CopyableFileList {
    const oneshotDict = this.seperateEachOneshot(
      analysedFiles.filter(({ isOneshot }) => isOneshot)
    );
    return this.generateSortedFiles(oneshotDict);
  }

  private seperateEachOneshot(
    analysedFiles: IFileAnalysis[]
  ): { [index: string]: IFileAnalysis[] } {
    const oneshotDict: { [index: string]: IFileAnalysis[] } = {};
    analysedFiles.forEach((analysedFile) => {
      const { oneshotType } = analysedFile;
      if (!oneshotDict[oneshotType]) oneshotDict[oneshotType] = [];
      oneshotDict[oneshotType].push(analysedFile);
    });
    return oneshotDict;
  }

  private generateSortedFiles(oneshotDict: {
    [index: string]: IFileAnalysis[];
  }): CopyableFileList {
    const sortedFiles: CopyableFileList = [];
    Object.keys(oneshotDict).forEach((oneshot) => {
      oneshotDict[oneshot].forEach(({ filename, filepath }) => {
        sortedFiles.push([
          filepath,
          this.generateSortedFilepath(filename, oneshot),
        ]);
      });
    });
    return sortedFiles;
  }

  private generateSortedFilepath(
    filename: string,
    oneshotType: string
  ): string {
    return path.join(
      this.masterDestinationPath,
      'oneshots',
      oneshotType,
      filename
    );
  }
}
