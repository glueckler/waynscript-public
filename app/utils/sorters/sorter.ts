import { IFileAnalysis } from '../analysers/IFileAnalysis';
import { BpmSorter } from './BpmSorter';
import { CopyableFileList } from '../filecopy';
import { ISorter } from './ISorter';
import { KeySorter } from './KeySorter';
import { OneshotSorter } from './OneshotSorter';

interface IComposedSorter {
  analysedFiles: IFileAnalysis[];
  sorter: ISorter;
  sort(): CopyableFileList;
}

export class Sorter implements IComposedSorter {
  static newBpmLoopsSorter(
    analysedFiles: IFileAnalysis[],
    masterDestinationPath: string
  ) {
    return new Sorter(
      analysedFiles,
      new BpmSorter(masterDestinationPath, {
        includeLoops: true,
        includeNonLoops: false,
      })
    );
  }

  static newBpmEtcSorter(
    analysedFiles: IFileAnalysis[],
    masterDestinationPath: string
  ) {
    return new Sorter(
      analysedFiles,
      new BpmSorter(masterDestinationPath, {
        includeLoops: false,
        includeNonLoops: true,
      })
    );
  }

  static newKeyLoopsSorter(
    analysedFiles: IFileAnalysis[],
    masterDestinationPath: string
  ) {
    return new Sorter(
      analysedFiles,
      new KeySorter(masterDestinationPath, {
        includeLoops: true,
        includeNonLoops: false,
      })
    );
  }

  static newKeyEtcSorter(
    analysedFiles: IFileAnalysis[],
    masterDestinationPath: string
  ) {
    return new Sorter(
      analysedFiles,
      new KeySorter(masterDestinationPath, {
        includeLoops: false,
        includeNonLoops: true,
      })
    );
  }

  static newOneshotSorter(
    analysedFiles: IFileAnalysis[],
    masterDestinationPath: string
  ) {
    return new Sorter(analysedFiles, new OneshotSorter(masterDestinationPath));
  }

  constructor(public analysedFiles: IFileAnalysis[], public sorter: ISorter) {}

  /**
   * sort
   */
  public sort() {
    return this.sorter.sort(this.analysedFiles);
  }
}
