export interface ISortingOptions {
  masterSortedDir: string;
}

// eslint-disable-next-line @typescript-eslint/naming-convention
class SORTING_OPTIONS implements ISortingOptions {
  constructor(public masterSortedDir: string) {}
}

// todo: figure this one out.. not hardcode
export default new SORTING_OPTIONS('/users/dbean/desktop/wayns');
