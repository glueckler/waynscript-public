import { BpmAnal } from '../../../app/utils/analysers/BpmAnal';
import { Result } from '../../../app/utils/analysers/Result';

describe('BpmAnal has correct properties', () => {
  const namesWithSingleBpm: [number, Result, string[]][] = [
    [
      128,
      Result.CERTAIN_FIND,
      ['VVE1 Vocoder Loops 128BPM', 'VVE1 Vocoder Loops 128bpm'],
    ],
    [
      92,
      Result.CERTAIN_FIND,
      [
        '92 BPM 100 Guitar Loops',
        '92 bpm Guitar Loops 85',
        'EFL-S2-92bpm-F#_68_Classical',
        'Emotionz 99 92bpm',
        '_splice_92BPM_top_loops',
        '_splice_92bpm_top_loops',
        '92 BPM - D',
        '92 BPM - D',
        '92BPM',
        '92 BPM',
        '092 bpm',
        '092-bpm',
        'Construction Chords 92 Bpm',
        'Construction Chords 92-Bpm',
        'Construction Chords 92_Bpm',
        'EFL-S2-92bpm-F#_68_Classical',
        'Emotionz 92bpm',
        'The 6th 92bpm',
        '101 92 BPM - D',
        '109 92 BPM - D',
        '92 BPM_100 - D',
      ],
    ],
    [
      92,
      Result.CERTAIN_FIND,
      [
        'BPM92',
        'BPM092',
        'BPM92',
        'BPM-92',
        'BPM_92',
        'bpm 92',
        'Bpm092_Darkener',
        'Bpm092_GettinLo',
        'bpm92 yaysample 101',
        'bpm 92 yaysample 101',
        'bpm-92 yaysample 101',
        'bpm_92 yaysample 101',
      ],
    ],
    [
      100,
      Result.UNCERTAIN_FIND,
      [
        'VVE1 Vocoder Loops 100',
        '100  Guitar Loops',
        '_splice_100_top_loops',
        '100  - D',
        '100',
        '100 ',
        '100-',
        '100_GettinLo',
      ],
    ],
    [
      65,
      Result.UNCERTAIN_FIND,
      [
        '065_Darkener',
        '065_GettinLo',
        'Construction Chords 65 ',
        'Construction Chords 65-',
        'EFL-S2-65-F#_68_Classical',
        'Emotionz 65',
        'The 6th 65',
      ],
    ],
  ];

  namesWithSingleBpm.forEach(([bpm, result, names]) => {
    describe(`Bpm: ${bpm}  ..  Certainty: ${result}`, () => {
      names.forEach((name) => {
        const anal = new BpmAnal(name);
        it(`${name} || ${anal.bpm.bpm} should be ${bpm}`, () => {
          expect(anal.bpm.bpm).toBe(bpm);
        });
        it(`${name} || ${anal.bpm.result} should be ${result}`, () => {
          expect(anal.bpm.result).toBe(result);
        });
      });
    });
  });

  const namesWithoutBpm: string[] = [
    'samplename (BPM Synced)',
    'samplename (BPM04)',
    'samplename (BPM200)',
    'samplename (BPM4)',
    'samplename (BPM 04)',
    'samplename (40BPM)',
    'samplename (49BPM)',
    'samplename 1400',
    'samplename 0880',
    'samplename 0120',
    'samplename 0120',
    'samplename 0120 44',
    'samplename 50',
    'samplename 200',
  ];

  namesWithoutBpm.forEach((name) => {
    it(`${name} should not have bpm`, () => {
      const anal = new BpmAnal(name);
      expect(anal.hasBpm).toBe(false);
    });
  });
});
