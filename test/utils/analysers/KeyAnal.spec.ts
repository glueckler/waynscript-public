import { KeyAnal } from '../../../app/utils/analysers/KeyAnal';
import { KeyKeys } from '../../../app/utils/analysers/KeyAnal';
import { Result } from '../../../app/utils/common';

describe('KeyAnal has correct properties', () => {
  const namesWithKey: [KeyKeys, Result, string[]][] = [
    [
      KeyKeys.NULL_KEY,
      Result.NO_FIND,
      ['Cool - Kick Drum 021 CB Radio _-_ sKick.wav'],
    ],
    [KeyKeys.G_FLAT_MIN, Result.CERTAIN_FIND, ['CL_ROCK_OneShot_5_92_Gbm.wav']],
    [
      KeyKeys.B_FLAT_MAJ,
      Result.CERTAIN_FIND,
      [
        '90_A#_A_Lead_01_SP',
        'sample in A#maj',
        'sample in A#major',
        'sample in A# maj',
        'Sample-Asharp',
        'Sample-AsharpMaj',
      ],
    ],
    [KeyKeys.B_FLAT_MIN, Result.CERTAIN_FIND, ['smple-A-sharp-min']],
    [
      KeyKeys.G_FLAT_MAJ,
      Result.CERTAIN_FIND,
      [
        '100_F#_PadSharp_01_SP',
        '100_Fsharp_PadSharp_01_SP',
        '100_F_sharp_PadSharp_01_SP',
        '100_F-sharp_PadSharp_01_SP',
        '100_F sharp_PadSharp_01_SP',
      ],
    ],
    [
      KeyKeys.E_FLAT_MAJ,
      Result.CERTAIN_FIND,
      [
        'Horny_House_125_Eflat_PL - PrimeHouse',
        'Horny_House_125_EflatMaj_PL - PrimeHouse',
        'Horny_House_125_E-flat_PL - PrimeHouse',
        'Horny_House_125_E flat_PL - PrimeHouse',
        'Horny_House_125_E flat_maj_PL - PrimeHouse',
        'Horny_House_125_E_flat_PL - PrimeHouse',
        'Bpm126_Eb_PillJill - PrimeHouse.wav',
        'Bpm126_eb_PillJill - PrimeHouse.wav',
      ],
    ],
    [
      KeyKeys.E_NAT_MIN,
      Result.CERTAIN_FIND,
      [
        'Bpm65_Em_Away_SawLead_01 - sUrbanDreams.wav',
        '125_Emin_Perc&Stab_SP01',
      ],
    ],
    [
      KeyKeys.E_NAT_MAJ,
      Result.CERTAIN_FIND,
      [
        'VUB1 Root E - 34',
        'Urban_E_C5',
        'EACV2BASS_20_E - sBass',
        'Low_Hoover__E___128bpm',
        'E_Justchill_Bass',
      ],
    ],
    [
      KeyKeys.E_NAT_MAJ,
      Result.CERTAIN_FIND,
      [
        'The_Meadow_91_Emaj_PL',
        'SynthLoop_173_D7-Emaj_PL',
        'SynthLoop_173_D7-Emajor_PL',
        'SynthLoop_173_D7-EMaj_PL',
        'SynthLoop_173_D7-EMajor_PL',
        'SynthLoop_173_D7-EMAJ_PL',
        'SynthLoop_173_D7-EMAJOR_PL',
      ],
    ],
    [KeyKeys.D_FLAT_MAJ, Result.CERTAIN_FIND, ['90_Db_CliveSynth_01_SP']],
    [
      KeyKeys.D_NAT_MIN,
      Result.CERTAIN_FIND,
      [
        'PHACTION_LEAD_New Orleans_174_Dm',
        'OS_OD_115_Dmin_Dark_Texture_Pad',
        'Nice Pad Dminor 140Bpm',
        '90 BPM D Min - sLofi',
      ],
    ],
  ];

  namesWithKey.forEach(([key, result, names]) => {
    describe(`Key: ${key}  ..  Certainty: ${result}`, () => {
      names.forEach((name) => {
        const anal = new KeyAnal(name);
        it(`${name} || ${anal.key.key} should be ${key}`, () => {
          expect(anal.key.key).toBe(key);
        });
        it(`${name} || ${anal.key.result} should be ${result}`, () => {
          expect(anal.key.result).toBe(result);
        });
      });
    });
  });

  const namesWithoutKey: string[] = [
    'Bpm126_keb_PillJill - PrimeHouse.wav',
    'Bpm126_ebo_PillJill - PrimeHouse.wav',
    'sample in a',
    'g house',
    'Apple - Bite 1 _-_ sFoley.wav',
    'BB Fruits and Veg',
  ];

  namesWithoutKey.forEach((name) => {
    it(`'${name}' should not have key`, () => {
      const anal = new KeyAnal(name);
      expect(anal.hasKey).toBe(false);
    });
  });
});
