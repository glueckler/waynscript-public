// import fs from 'fs';
import path from 'path';
// import _ from 'lodash';

import { TagAnal } from '../../../app/utils/analysers/TagAnal';
import { tagDataDictionary } from '../../../app/utils/analysers/tagDataDictionary';
import recursiveFileList from '../../../app/utils/recursiveFileList';

import { audioExts } from '../../../app/constants/audioExts';
import { MASTER_SAMPLE_DIR } from '../../../app/envVars';

const sampleDir = MASTER_SAMPLE_DIR;
if (!sampleDir) {
  throw new Error('Doesnt seem the sample dir is coming through..');
}

// const printMatch = () => {
//   const outputFilePath = `${__dirname}/Tag.printMatch.OUTPUT.txt`;

//   try {
//     fs.readFileSync(outputFilePath);
//   } catch (error) {
//     console.warn(`Creating new OUTPUT.txt at ${outputFilePath}`);
//     fs.writeFileSync(outputFilePath, '');
//   }

//   console.log(`sampleDir ==> ${sampleDir}`);
//   const filelist = recursiveFileList(sampleDir, audioExts);

//   console.log(`OUTPUT.txt ==> ${outputFilePath}`);

//   const filestring: string[] = [];

//   filelist.forEach((filepath) => {
//     const filename = path.basename(filepath);
//     const tagged = new Tag(filename);
//     if (tagged.hasTag && tagged.tags) {
//       filestring.push(`${filename}   ...   ${tagged.tags.toString()}`);
//     }
//   });
//   fs.writeFileSync(outputFilePath, _.shuffle(filestring).join('\n\n'));
// };

const tagHelper = () => {
  console.log(`sampleDir ==> ${sampleDir}`);

  const filelist = recursiveFileList(sampleDir, audioExts).map((fp) =>
    path.basename(fp)
  );

  Object.values(tagDataDictionary).forEach((tagData) => {
    if (!tagData) return;
    const { tagId, development } = tagData;
    if (!tagId) return;
    if (!development || development.development === false) return;

    const filteredFileList = filelist.filter((f) =>
      f.toLowerCase().includes(development.massSearch.toLowerCase())
    );

    TagAnal.developmentMassSearch(
      filteredFileList,
      tagId,
      development.massSearch
    );
  });
};

tagHelper();
