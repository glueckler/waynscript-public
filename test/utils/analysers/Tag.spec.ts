import { TagAnal } from '../../../app/utils/analysers/TagAnal';
import { TagIds } from '../../../app/utils/analysers/tagDataDictionary';
import { Result } from '../../../app/utils/common';

const namesWithSingleTag: [TagIds, Result, string[]][] = [
  [
    TagIds.CLAP,
    Result.CERTAIN_FIND,
    ['clap.wav', 'clp.wav', '46_Claps_02_SP.wav', '46_Clps_02_SP.wav'],
  ],
  [
    TagIds.CLAP,
    Result.CERTAIN_FIND,
    [
      '90_PercDryClap_01_SP.wav',
      '808CLAP1.wav',
      'SHD_808CLP_22.wav',
      'SHD_808CLP22.wav',
      '100RackzClp III.wav',
    ],
  ],
  [TagIds.CLAP, Result.UNCERTAIN_FIND, ['909ClapV2_Orig']],
  [
    TagIds.KICK,
    Result.CERTAIN_FIND,
    [
      'kick.wav',
      'kicks23.wav',
      '- his - Baby Kick - skick19 --kkicks.wav',
      'mad Kick1.wav',
      '04_Trap_Kick_Industry_Kick --kkicks',
      'Attack_Kick09.wav',
      'A_Kick-09.wav',
      'A_Kick 09.wav',
      'Bounce 120bpm_(Kick 2).wav',
      'just blaze d.r.kick - sdrumsJustBlaze',
    ],
  ],
  [
    TagIds.KICK,
    Result.CERTAIN_FIND,
    [
      '36kicks.wav',
      '808 JayKodeChiefKick + 808 - sBass.wav',
      'JS_B_KICKCYMSTOP_1 - sloopmasters',
    ],
  ],
  [TagIds.KICK, Result.UNCERTAIN_FIND, ['VEC3 Bassdrums Trance 55']],
  [
    TagIds.SNARE,
    Result.CERTAIN_FIND,
    [
      'snare.wav',
      'somethings snares3.wav',
      'Snare.wav',
      'sizzle snare.wav',
      '100Million_Snare_02.wav',
    ],
  ],
  [TagIds.SNARE, Result.CERTAIN_FIND, ['125_OnlySnareSounds_SP01.wav']],
  [TagIds.ROLL, Result.CERTAIN_FIND, ['AB_BRE 10 Snare Roll.wav']],
  [
    TagIds.ROLL,
    Result.CERTAIN_FIND,
    ['TrapSnareRoll.wav', 'Bpm063_TrapRoll14', 'Bpm063_TrapRolls14'],
  ],
  [TagIds.ROLL, Result.UNCERTAIN_FIND, ['Bpm063_TrapRollSauce14']],
  [
    TagIds.HIHAT,
    Result.CERTAIN_FIND,
    [
      'hi hat',
      'hi_hat',
      'Hi_Hat',
      'hihat.wav',
      'somethings hihats3.wav',
      'hihat.wav',
      'sizzle hihat.wav',
      '100Million_hihat_02.wav',
      'HI Hat.wav',
      'somethings HI Hats3.wav',
      'HI Hat.wav',
      'sizzle HI Hat.wav',
      '100Million_HI Hat_02.wav',
    ],
  ],
  [
    TagIds.HIHAT,
    Result.CERTAIN_FIND,
    [
      'sweet hat',
      'sweet_hat',
      'hat.wav',
      'somethings hats3.wav',
      'sizzle hat.wav',
      '100Million_hat_02.wav',
    ],
  ],
  [
    TagIds.HIHAT,
    Result.UNCERTAIN_FIND,
    [
      'HHLoop_Youngsters4_79.5BPM.wav',
      'VEC1 SpecialHH 01.wav',
      'EQ-HhC092.wav',
    ],
  ],
  [
    TagIds.CLOSED_HIHAT,
    Result.CERTAIN_FIND,
    [
      'closed hihat.wav',
      'closed hihats 3.wav',
      'closedhihat',
      'closed_hh',
      'closed hh',
      'closedhh',
      'hihat_closed',
      'hh closed.wav',
      'Bpm76_G#m_100Million_ClosedHatz_Intro_01',
      'Cymatics - Eternity Closed Hihat 6 - sLofi',
      'CyOn - Closed Hat - 28',
      'Finish_ClosedHat_01',
      'HIHAT10(CLOSED).wav',
    ],
  ],
  [TagIds.CLOSED_HIHAT, Result.CERTAIN_FIND, ['36closedhat --hhats']],
  [
    TagIds.CLOSED_HIHAT,
    Result.UNCERTAIN_FIND,
    ['BUUNSHIN_PERCUSSION_HatLoopClosed_174BPM.wav', 'EQ-HhC092.wav'],
  ],
  [
    TagIds.OPEN_HIHAT,
    Result.CERTAIN_FIND,
    [
      'Open hihat.wav',
      'Open hihats 3.wav',
      'Openhihat',
      'Open_hh',
      'Open hh',
      'Openhh',
      'hihat_Open',
      'hh Open.wav',
      'Bpm76_G#m_100Million_OpenHatz_Intro_01',
      'Cymatics - Eternity Open Hihat 6 - sLofi',
      'CyOn - Open Hat - 28',
      'Finish_OpenHat_01',
      'HIHAT10(OPEN)',
    ],
  ],
  [TagIds.OPEN_HIHAT, Result.CERTAIN_FIND, ['36openhat --hhats']],
  [
    TagIds.OPEN_HIHAT,
    Result.UNCERTAIN_FIND,
    ['EQ-Hho092.wav', '_HatLoopOpen_174BPM.wav'],
  ],
  [
    TagIds.CRASH,
    Result.CERTAIN_FIND,
    [
      'VEC3 Cymbals Crash 26',
      'CJ_cymbal_crash_small',
      'TrTrap2 - FX - Crash FX - 37',
      'Cymatics - Crash 25',
    ],
  ],
  [
    TagIds.TOM,
    Result.CERTAIN_FIND,
    ['Rack Tom', 'Genius_Toms04 - hiphopDrumHits', 'td6k tom 84'],
  ],
  [
    TagIds.CLAVE,
    Result.CERTAIN_FIND,
    [
      'SO_TP_clave_dry',
      'Percussion Clave 002 808 --pperc',
      'wa_808tape_clave_02_clean _-_ sPercussion',
    ],
  ],
  [
    TagIds.PERCUSSION,
    Result.CERTAIN_FIND,
    ['Militia_Perc01', 'Genius_Percussion02'],
  ],
  [TagIds.PERCUSSION, Result.CERTAIN_FIND, ['PERC6HER']],
  [
    TagIds.BREAK,
    Result.CERTAIN_FIND,
    ['GRIDLOK_Break_Shuffled Up Amen --bbreaks'],
  ],
  [TagIds.BREAK, Result.CERTAIN_FIND, ['BREAK18SFES']],
  [
    TagIds.RIDE,
    Result.CERTAIN_FIND,
    ['Loop44 kick03 B.C. Ride 106 BPM', 'VMH1 Ride 02 --rride'],
  ],
  [
    TagIds.SHAKER,
    Result.CERTAIN_FIND,
    [
      'MM_2099_percussion_shaker_ting_01 --pperc',
      'Shaker_1stStreet2 --pperc',
      'Takeova_Shaker06',
    ],
  ],
  [
    TagIds.EIGHTOHEIGHT,
    Result.CERTAIN_FIND,
    [
      'Classic 808 Clap --cclaps',
      '808 + Kick mixed C3 --kkicks',
      'Messy 808 C3 --kkicks',
    ],
  ],
  [
    TagIds.CONGA,
    Result.CERTAIN_FIND,
    ['BONGO-CONGA11', 'Congas9 - sEthnicOneShots', 'Conga_15_SP _-_ sConga'],
  ],
  [
    TagIds.RIM,
    Result.CERTAIN_FIND,
    ['Genius_Rim01', 'Rim Shots - Dance Girl', 'Loop69 rimshot02 Sticky Dr 79'],
  ],
  [
    TagIds.BONGO,
    Result.CERTAIN_FIND,
    ['bongo2', 'BONGO-CONGA112 --cconga', 'TD6K_006_Bongo_SP _-_ sPercussion'],
  ],
  [
    TagIds.TAM,
    Result.CERTAIN_FIND,
    [
      'TAM9 _-_ sHiHat',
      'Real Quick 110bpm_Tamb',
      'Mafia_Tambourine02 - greatestDrumHits',
    ],
  ],
  [
    TagIds.SNAP,
    Result.CERTAIN_FIND,
    [
      'SHIFTEE_percussion_wy_snap_wet',
      'Skull_Snaps_-_Its_A_New_Day - sbreaks',
      'Unsorted - Snap 005 Lex - ssnare19',
    ],
  ],
  [
    TagIds.FILL,
    Result.CERTAIN_FIND,
    [
      'Satl_Fx_Fill --ffxs',
      'Ralph_Carmichael_-_The_Addicts_Psalm_(Fill_3)',
      'KS - Usual Suspects fill',
    ],
  ],
  [
    TagIds.BELL,
    Result.CERTAIN_FIND,
    [
      'MM_2099_percussion_bell',
      'Bell A 6',
      'TSG_Bells___Plucks_One_Shot_Manderin_C',
    ],
  ],
  [
    TagIds.BELL,
    Result.CERTAIN_FIND,
    [
      'RARE_percussion_tibetBells_trap_texture_75_loop6',
      'SewersBell_01_SP - sOneShotSamplePhonics',
    ],
  ],
  [
    TagIds.CAJON,
    Result.CERTAIN_FIND,
    [
      'RARE_percussion_cajon_drum_shot6',
      'Cajon41 - sEthnicOneShots',
      'RARE_percussion_cajon_drumkit_spain_dope_85_loop8',
    ],
  ],
];

const namesWithMultipleTags: [TagIds[], string[]][] = [
  [
    [TagIds.CLOSED_HIHAT, TagIds.HIHAT],
    [
      '36closedhat --hhats',
      'closed hihat',
      'closedhihat',
      'closed_hh',
      'hihat_closed',
      'hh closed',
      'Cymatics - Eternity Closed Hihat 6.wav',
      'CyOn - Closed Hat - 28',
      'Finish_ClosedHat_01',
      'BUUNSHIN_PERCUSSION_HatLoopClosed_174BPM.wav',
    ],
  ],
  [[TagIds.KICK, TagIds.SNARE], ['GFAT_120_kick_snare_loop_klimb']],
  [[TagIds.CLAP, TagIds.SNARE], ['VMH1 Claps & Snares 250']],
];

const tricky: [TagIds, string[]][] = [
  [
    TagIds.KICK,
    [
      'Bpm142_BrainFreeze_NoKick04.wav',
      'loop with no kick.wav',
      "Sample - Kickin' Hiphop Beats 08",
      'JVIEWS_clock_shop_klutzy_disco_no_kick_90',
      'kickindouble',
    ],
  ],
  [
    TagIds.SNARE,
    ['AB_BRE 10 Snare Roll.wav', 'Bpm063_TrapRoll14 - sTrapSnareRolls'],
  ],
  [TagIds.HIHAT, ['HHI2_96_Chimes_1_Lp.wav']],
];

const tagCertaintyTest = ([tagId, tagCertainty, names]) => {
  names.forEach((name) => {
    const analyzed = new TagAnal(name);
    it(`Name: '${name}' hasTag property should be true`, () => {
      expect(analyzed.hasTag).toBe(true);
    });
    it(`Name: '${name}' should have tag ${tagId}`, () => {
      expect(analyzed.tags[tagId]).not.toBeUndefined();
    });
    it(`Name: '${name}' Tag (${tagId}) result should be: ${tagCertainty}`, () => {
      expect(analyzed.tags[tagId].result).toEqual(tagCertainty);
    });
  });
};
const multipleTagsTest = ([tagIds, names]) => {
  names.forEach((name) => {
    const analyzed = new TagAnal(name);
    it(`Name: ${name} hasTag property should be true`, () => {
      expect(analyzed.hasTag).toBe(true);
    });
    it(`Name '${name}' should have all tags ${tagIds.join(' & ')}`, () => {
      expect(Object.keys(analyzed.tags)).toEqual(
        expect.arrayContaining(tagIds)
      );
    });
  });
};
const trickyNamesTest = ([tagId, names]) => {
  names.forEach((name) => {
    const analyzed = new TagAnal(name);
    it(`Name: ${name} should not have tag ${tagId}`, () => {
      expect(analyzed.tagIds).not.toContain(tagId);
    });
  });
};

describe('Tag (Analyzer) class', () => {
  describe('Drum Tags', () => {
    describe('Properties are correct', () => {
      describe('Correct tag result and tag identification', () => {
        namesWithSingleTag.forEach(tagCertaintyTest);
      });

      describe('filenames with multiple tags', () => {
        namesWithMultipleTags.forEach(multipleTagsTest);
      });

      describe('tricky filenames', () => {
        tricky.forEach(trickyNamesTest);
      });
    });
  });
});
