/* eslint-disable global-require */
import { DirectoryAnalysis } from '../../../app/utils/analysers/DirectoryAnalysis';

jest.mock('fs');
describe('Directory Analysis Class', () => {
  describe('constructor functionality', () => {
    const MOCK_FILE_INFO = [
      {
        name: '/path/to/file1.js',
        isDirectory() {
          return false;
        },
      },
      {
        name: '/path/to/file2.txt',
        isDirectory() {
          return false;
        },
      },
      {
        name: '/path/to/kick.wav',
        isDirectory() {
          return false;
        },
      },
      {
        name: '/path/to/snare.wav',
        isDirectory() {
          return false;
        },
      },
      {
        name: '/path/to/samples directory within',
        isDirectory() {
          return true;
        },
      },
    ];

    beforeEach(() => {
      // Set up some mocked out file info before each test
      require('fs').__setMockFiles(MOCK_FILE_INFO);
    });

    it('should have a correst list of files and directories', () => {
      const defaultDirNameAnalysis = {
        hasBpm: false,
        hasKey: false,
        isLoop: false,
        isOneshot: false,
        bpm: 0,
        key: '',
      };
      const analysed = new DirectoryAnalysis(
        '/path/to',
        defaultDirNameAnalysis
      );
      expect(analysed.filesList).toContain('kick.wav');
      expect(analysed.filesList).toContain('snare.wav');
      expect(analysed.filesList).not.toContain('file2.txt');
      expect(analysed.filesList).not.toContain('file1.js');
      expect(analysed.dirsList).toContain('samples directory within');
      expect(analysed.dirsList.length).toBe(1);
      expect(analysed.filesList.length).toBe(2);
    });
  });

  describe('directory analysis on the directory name', () => {
    const defaultDirNameAnalysis = {
      hasBpm: false,
      hasKey: false,
      isLoop: false,
      isOneshot: false,
      bpm: 0,
      key: '',
    };
    it('should assume nothing on a standard directory name', () => {
      const parentDir = 'samples library folder';
      const analysedParentDir = new DirectoryAnalysis(
        parentDir,
        defaultDirNameAnalysis
      );
      expect(analysedParentDir.directoryNameAnalysis.hasBpm).toBe(false);
      expect(analysedParentDir.directoryNameAnalysis.hasKey).toBe(false);
      expect(analysedParentDir.directoryNameAnalysis.isLoop).toBe(false);
      expect(analysedParentDir.directoryNameAnalysis.isOneshot).toBe(false);
      expect(analysedParentDir.directoryNameAnalysis.key).toBe('');
      expect(analysedParentDir.directoryNameAnalysis.bpm).toBe(0);
    });
    it('should catch loops, key, and bpm', () => {
      const parentDir = 'guitar loops in Ab at 110bpm';
      const analysedParentDir = new DirectoryAnalysis(
        parentDir,
        defaultDirNameAnalysis
      );
      expect(analysedParentDir.directoryNameAnalysis.hasBpm).toBe(true);
      expect(analysedParentDir.directoryNameAnalysis.hasKey).toBe(true);
      expect(analysedParentDir.directoryNameAnalysis.isLoop).toBe(true);
      expect(analysedParentDir.directoryNameAnalysis.isOneshot).toBe(false);
      expect(analysedParentDir.directoryNameAnalysis.key).toBe('Ab');
      expect(analysedParentDir.directoryNameAnalysis.bpm).toBe(110);
    });

    it('pass on analysis values from the parent', () => {
      const parentDir = 'drum loops at 99bpm';
      const analysedParentDir = new DirectoryAnalysis(
        parentDir,
        defaultDirNameAnalysis
      );
      const childDir = 'more samples in here';
      const childDirAnalysys = new DirectoryAnalysis(
        childDir,
        analysedParentDir.directoryNameAnalysis
      );
      expect(childDirAnalysys.directoryNameAnalysis.hasBpm).toBe(true);
      expect(childDirAnalysys.directoryNameAnalysis.hasKey).toBe(false);
      expect(childDirAnalysys.directoryNameAnalysis.isLoop).toBe(true);
      expect(childDirAnalysys.directoryNameAnalysis.isOneshot).toBe(false);
      expect(childDirAnalysys.directoryNameAnalysis.key).toBe('');
      expect(childDirAnalysys.directoryNameAnalysis.bpm).toBe(99);
    });

    it('pass on analysis values from the parent (with oneshot)', () => {
      const parentDir = 'kicks';
      const analysedParentDir = new DirectoryAnalysis(
        parentDir,
        defaultDirNameAnalysis
      );
      const childDir = 'the spicey ones';
      const childDirAnalysys = new DirectoryAnalysis(
        childDir,
        analysedParentDir.directoryNameAnalysis
      );
      expect(childDirAnalysys.directoryNameAnalysis.hasBpm).toBe(false);
      expect(childDirAnalysys.directoryNameAnalysis.hasKey).toBe(false);
      expect(childDirAnalysys.directoryNameAnalysis.isLoop).toBe(false);
      expect(childDirAnalysys.directoryNameAnalysis.isOneshot).toBe(true);
      expect(childDirAnalysys.directoryNameAnalysis.key).toBe('');
      expect(childDirAnalysys.directoryNameAnalysis.bpm).toBe(0);
    });
  });

  describe('file analysis list', () => {
    const MOCK_FILE_INFO = [
      {
        name: '/path/to/file1.js',
        isDirectory() {
          return false;
        },
      },
      {
        name: '/path/to/file2.txt',
        isDirectory() {
          return false;
        },
      },
      {
        name: '/path/to/kick.wav',
        isDirectory() {
          return false;
        },
      },
      {
        name: '/path/to/snare.wav',
        isDirectory() {
          return false;
        },
      },
      {
        name: '/path/to/samples directory within',
        isDirectory() {
          return true;
        },
      },
      {
        name: '/path/to/samples directory within/another kick.wav',
        isDirectory() {
          return false;
        },
      },
      {
        name: '/path/to/samples directory within/hihat loop.wav',
        isDirectory() {
          return false;
        },
      },
      {
        name: '/path/to/samples directory within/75bpm vocal.wav',
        isDirectory() {
          return false;
        },
      },
      {
        name: '/path/to/drum loops',
        isDirectory() {
          return true;
        },
      },
      {
        name: '/path/to/drum loops/sample1.wav',
        isDirectory() {
          return false;
        },
      },
      {
        name: '/path/to/drum loops/sample2.wav',
        isDirectory() {
          return false;
        },
      },
    ];

    beforeEach(() => {
      // Set up some mocked out file info before each test
      require('fs').__setMockFiles(MOCK_FILE_INFO);
    });

    it('should create a list with the right length', () => {
      const defaultDirNameAnalysis = {
        hasBpm: false,
        hasKey: false,
        isLoop: false,
        isOneshot: false,
        bpm: 0,
        key: '',
      };
      const analysed = new DirectoryAnalysis(
        '/path/to',
        defaultDirNameAnalysis
      );
      expect(analysed.fileAnalysisList.length).toBe(7);
      // test first result
      expect(analysed.fileAnalysisList[0].filepath).toBe(
        '/path/to/samples directory within/another kick.wav'
      );
      expect(analysed.fileAnalysisList[0].isOneshot).toBe(true);
      expect(analysed.fileAnalysisList[0].hasBpm).toBe(false);
      // test another result
      expect(analysed.fileAnalysisList[6].filepath).toBe('/path/to/snare.wav');
      expect(analysed.fileAnalysisList[6].isOneshot).toBe(false);
      expect(analysed.fileAnalysisList[6].hasBpm).toBe(false);
      // test that samples in the loops directory are loops
      expect(analysed.fileAnalysisList[3].isLoop).toBe(true);
    });
  });
});
