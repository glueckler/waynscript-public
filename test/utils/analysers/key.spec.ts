import { KeyAnal } from '../../../app/utils/analysers/KeyAnal';

describe('Key analyser', () => {
  describe('hasKey property', () => {
    describe('should be true for these filenames..', () => {
      const namesWithKey = [
        'sample in A',
        'sample in B',
        'sample in C',
        'sample in D',
        'sample in E',
        'sample in F',
        'sample in G',
        'sample in Am',
        'sample in am',
        'sample in Amin',
        'sample in Aminor',
        'sample in A minor',
        'sample in AMaj',
        'sample in AMajor',
        'sample in Ab',
        'sample in Aflat',
        'sample in A flat',
        'sample in A#',
        'sample in Asharp',
        'sample in A sharp',
        'sample in Ab min',
        'sample in Ab Maj',
        'sample in Abmin',
        'sample in AbMaj',
        'sample in Abm',
        'sample in Aflat min',
        'sample in Aflat Maj',
        'sample in A flat min',
        'sample in A flat Maj',
        'sample in A# min',
        'sample in A# Maj',
        'sample in A#min',
        'sample in A#Maj',
        'sample in A#m',
        'sample in Asharp min',
        'sample in Asharp Maj',
        'sample in A sharp min',
        'sample in A sharp Maj',
        '100_F#_PadSharp_01_SP',
        '100_Fsharp_PadSharp_01_SP',
        '100_F_sharp_PadSharp_01_SP',
        '100_F-sharp_PadSharp_01_SP',
        '100_F sharp_PadSharp_01_SP',
        'Horny_House_125_Eflat_PL - PrimeHouse',
        'Horny_House_125_E-flat_PL - PrimeHouse',
        'Horny_House_125_E flat_PL - PrimeHouse',
        'Horny_House_125_E_flat_PL - PrimeHouse',
        'Bpm126_Eb_PillJill - PrimeHouse.wav',
        'Bpm126_eb_PillJill - PrimeHouse.wav',
        'Bpm65_Em_Away_SawLead_01 - sUrbanDreams.wav',
        '125_Emin_Perc&Stab_SP01',
        'VUB1 Root E - 34',
        'Urban_E_C5',
        'EACV2BASS_20_E - sBass',
        'The_Meadow_91_Emaj_PL',
        'SynthLoop_173_D7-Emaj_PL',
        'SynthLoop_173_D7-Emajor_PL',
        'SynthLoop_173_D7-EMaj_PL',
        'SynthLoop_173_D7-EMajor_PL',
        'SynthLoop_173_D7-EMAJ_PL',
        'SynthLoop_173_D7-EMAJOR_PL',
        'Low_Hoover__E___128bpm',
        'E_Justchill_Bass',
        'GS_TNCL_85_E#m_ATMO_MULEN',
        '90_Db_CliveSynth_01_SP',
        'PHACTION_LEAD_New Orleans_174_Dm',
        'OS_OD_115_Dmin_Dark_Texture_Pad',
        'Nice Pad Dminor 140Bpm',
        '90 BPM D Min - sLofi',
      ];

      namesWithKey.forEach((name) => {
        it(`${name}`, () => {
          const keyed = new Key(name);
          expect(keyed.hasKey()).toBe(true);
        });
      });
    });
    describe('should be false for these filenames..', () => {
      const namesWithoutKey = [
        'Bpm126_keb_PillJill - PrimeHouse.wav',
        'Bpm126_ebo_PillJill - PrimeHouse.wav',
        'sample in a',
        'g house',
        'Apple - Bite 1 _-_ sFoley.wav',
        'BB Fruits and Veg',
      ];

      namesWithoutKey.forEach((name) => {
        it(`${name}`, () => {
          const keyed = new Key(name);
          expect(keyed.hasKey()).toBe(false);
        });
      });
    });
  });

  describe('key property, should have the right key', () => {
    const nameAndKeys = [
      ['90_A#_A_Lead_01_SP', 'A#'],
      ['100_F#_PadSharp_01_SP', 'F#'],
      ['100_Fsharp_PadSharp_01_SP', 'F#'],
      ['100_F_sharp_PadSharp_01_SP', 'F#'],
      ['100_F-sharp_PadSharp_01_SP', 'F#'],
      ['100_F sharp_PadSharp_01_SP', 'F#'],
      ['Horny_House_125_Eflat_PL - PrimeHouse', 'Eb'],
      ['Horny_House_125_E-flat_PL - PrimeHouse', 'Eb'],
      ['Horny_House_125_E flat_PL - PrimeHouse', 'Eb'],
      ['Horny_House_125_E_flat_PL - PrimeHouse', 'Eb'],
      ['Bpm126_Eb_PillJill - PrimeHouse.wav', 'Eb'],
      ['Bpm126_eb_PillJill - PrimeHouse.wav', 'Eb'],
      ['Bpm65_Em_Away_SawLead_01 - sUrbanDreams.wav', 'Emin'],
      ['125_Emin_Perc&Stab_SP01', 'Emin'],
      ['VUB1 Root E - 34', 'E'],
      ['Urban_E_C5', 'E'],
      ['EACV2BASS_20_E - sBass', 'E'],
      ['The_Meadow_91_Emaj_PL', 'E'],
      ['SynthLoop_173_D7-Emaj_PL', 'E'],
      ['SynthLoop_173_D7-Emajor_PL', 'E'],
      ['SynthLoop_173_D7-EMaj_PL', 'E'],
      ['SynthLoop_173_D7-EMajor_PL', 'E'],
      ['SynthLoop_173_D7-EMAJ_PL', 'E'],
      ['SynthLoop_173_D7-EMAJOR_PL', 'E'],
      ['Low_Hoover__E___128bpm', 'E'],
      ['E_Justchill_Bass', 'E'],
      ['GS_TNCL_85_E#m_ATMO_MULEN', 'E#min'],
      ['90_Db_CliveSynth_01_SP', 'Db'],
      ['PHACTION_LEAD_New Orleans_174_Dm', 'Dmin'],
      ['OS_OD_115_Dmin_Dark_Texture_Pad', 'Dmin'],
      ['Nice Pad Dminor 140Bpm', 'Dmin'],
      ['90 BPM D Min - sLofi', 'Dmin'],
    ];

    nameAndKeys.forEach(([sample, key]) => {
      it(`sample: ${sample} ---> ${key}`, () => {
        const keyed = new Key(sample);
        expect(keyed.key()).toEqual(key);
      });
    });
  });
});
