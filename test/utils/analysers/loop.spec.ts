import { LoopAnal } from '../../../app/utils/analysers/LoopAnal';

describe('loop analyser', () => {
  describe('isLoop should be true..', () => {
    const namesWithloops = [
      'drum loops',
      'drum Loops',
      'DRUM LOOPS',
      'some_randomloops_maybe',
      'synths loops Ab 85bpm',
      'musical loop things',
      'primeloops',
      'percussionloops',
      'prime loops',
      'shakerloops',
      'Synth_loops',
      'Melody loop 5',
      'Loop Masters',
    ];

    namesWithloops.forEach((name) => {
      it(`should be true for: ${name}`, () => {
        const analysed = new Loop(name);
        expect(analysed.isLoop()).toBe(true);
      });
    });

    describe('isLoop should be false..', () => {
      const namesWithoutLoops = [
        'drumloopsicle',
        'some_randomlops_maybe',
        'synths Ab 85bpm',
        'musicalloopthings',
        'primel o o p s',
        'percussionlops',
        'poop Masters',
      ];

      namesWithoutLoops.forEach((name) => {
        it(`should be false for: ${name}`, () => {
          const analysed = new Loop(name);
          expect(analysed.isLoop()).toBe(false);
        });
      });
    });
  });
});
