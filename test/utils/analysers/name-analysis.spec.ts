describe('Analyser Class', () => {
  let filename;
  it(`should have the filename ${filename}`, () => {
    filename = 'kicks';
    const analysis = new NameAnalysis(filename);
    expect(analysis.filename).toEqual(filename);
  });

  it(`should have correct properties for filename: ${filename}`, () => {
    filename = 'kicks';
    const analysis = new NameAnalysis(filename);
    expect(analysis.hasBpm).toBe(false);
    expect(analysis.hasKey).toBe(false);
    expect(analysis.isOneshot).toBe(true);
    expect(analysis.isLoop).toBe(false);
  });
  it(`should have correct properties for filename: ${filename}`, () => {
    filename = 'guitar 100bpm loops';
    const analysis = new NameAnalysis(filename);
    expect(analysis.hasBpm).toBe(true);
    expect(analysis.bpm).toEqual(100);
    expect(analysis.hasKey).toBe(false);
    expect(analysis.isOneshot).toBe(false);
    expect(analysis.isLoop).toBe(true);
  });
  it(`should have correct properties for filename: ${filename}`, () => {
    filename = 'bass loops in Abminor';
    const analysis = new NameAnalysis(filename);
    expect(analysis.hasBpm).toBe(false);
    expect(analysis.hasKey).toBe(true);
    expect(analysis.key).toEqual('Abmin');
    expect(analysis.isOneshot).toBe(false);
    expect(analysis.isLoop).toBe(true);
  });
  it(`should have correct properties for filename: ${filename}`, () => {
    filename = 'emin piano at 110bpm';
    const analysis = new NameAnalysis(filename);
    expect(analysis.hasBpm).toBe(true);
    expect(analysis.bpm).toEqual(110);
    expect(analysis.hasKey).toBe(true);
    expect(analysis.key).toEqual('Emin');
    expect(analysis.isOneshot).toBe(false);
    expect(analysis.isLoop).toBe(false);
  });
});
