import { Oneshot } from '../../../app/utils/analysers/oneshot';

describe('oneshot analyser', () => {
  describe('isOneShot() function', () => {
    const oneshotNames = ['kicks', 'kickdrums', 'snares', 'hihats'];
    oneshotNames.forEach((name) => {
      it(`should be true for ${name}`, () => {
        const analysed = new Oneshot(name);
        expect(analysed.isOneshot()).toBe(true);
      });
    });

    const notOneshotNames = ['notkicksmaybe', 'nothing here'];
    notOneshotNames.forEach((name) => {
      it(`should be false for ${name}`, () => {
        const analysed = new Oneshot('name');
        expect(analysed.isOneshot()).toBe(false);
      });
    });
  });

  describe('one shot type', () => {
    const oneShotTypes = [
      ['kicks', 'kicks'],
      ['snares', 'snares'],
      ['hihats', 'hihats'],
    ];
    oneShotTypes.forEach(([filename, type]) => {
      it(`filename ${filename}, should be type '${type}'`, () => {
        const analysed = new Oneshot(filename);
        expect(analysed.oneshotType()).toEqual(type);
      });
    });
  });
});
