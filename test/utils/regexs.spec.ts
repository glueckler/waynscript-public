import { optionalSpace } from '../../app/utils/regexs';

describe('regex utils should pass..', () => {
  describe('optionalSpace', () => {
    const optionalSpaceMatches = [' ', '_', '-'];
    optionalSpaceMatches.forEach((match) => {
      it(`should match: '${match}'`, () => {
        expect(optionalSpace.exec(match)).not.toBeNull();
      });
    });

    const optionalSpaceMatchesFirst = [
      ['--', '-'],
      ['  ', ' '],
      ['__', '_'],
    ];
    optionalSpaceMatchesFirst.forEach(([test, match]) => {
      it(`should match only the first when testing ${test}`, () => {
        expect(optionalSpace.exec(test)[0]).toEqual(match);
      });
    });
  });
});
