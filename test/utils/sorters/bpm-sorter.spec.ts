import { BpmSorter } from '../../../app/utils/sorters/BpmSorter';

describe('BpmSorter class', () => {
  describe('sort() method', () => {
    it('should sort this analysis list correctly..', () => {
      const samplesAt70x99 = [...Array(99)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 70,
        hasBpm: true,
      }));
      const samplesAt71x101 = [...Array(101)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 71,
        hasBpm: true,
      }));
      const samplesAt72x50 = [...Array(50)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 72,
        hasBpm: true,
      }));
      //
      // make sure bpm ranges dont get too big..
      const samplesAt100x5 = [...Array(5)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 100,
        hasBpm: true,
      }));
      const samplesAt101x5 = [...Array(5)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 101,
        hasBpm: true,
      }));
      const samplesAt102x5 = [...Array(5)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 102,
        hasBpm: true,
      }));
      const samplesAt103x5 = [...Array(5)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 103,
        hasBpm: true,
      }));
      const samplesAt104x5 = [...Array(5)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 104,
        hasBpm: true,
      }));
      const samplesAt105x5 = [...Array(5)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 105,
        hasBpm: true,
      }));
      const samplesAt106x5 = [...Array(5)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 106,
        hasBpm: true,
      }));
      const samplesAt144x5 = [...Array(5)].map((_, index) => ({
        filename: `${index}sample-doubletime.wav`,
        filepath: `src/${index}sample-doubletime.wav`,
        bpm: 144,
        hasBpm: true,
      }));
      const fileAnalysisList = [
        ...samplesAt70x99,
        ...samplesAt71x101,
        ...samplesAt72x50,
        ...samplesAt100x5,
        ...samplesAt101x5,
        ...samplesAt102x5,
        ...samplesAt103x5,
        ...samplesAt104x5,
        ...samplesAt105x5,
        ...samplesAt106x5,
        ...samplesAt144x5,
      ];
      const bpmsort = new BpmSorter('masterpath', {
        includeLoops: true,
        includeNonLoops: true,
      });
      const sortedList = bpmsort.sort(fileAnalysisList);

      expect(sortedList.length).toBe(fileAnalysisList.length);
      expect(
        sortedList.filter(([, destpath]) => destpath.includes('/140-142/'))
      ).toHaveLength([...samplesAt70x99, ...samplesAt71x101].length);
      expect(
        sortedList.filter(([, destpath]) => destpath.includes('/144/'))
      ).toHaveLength([...samplesAt72x50, ...samplesAt144x5].length);
      expect(
        sortedList.filter(([, destpath]) => destpath.includes('/100-104/'))
      ).toHaveLength(
        [
          ...samplesAt100x5,
          ...samplesAt101x5,
          ...samplesAt102x5,
          ...samplesAt103x5,
          ...samplesAt104x5,
        ].length
      );
      expect(
        sortedList.filter(([, destpath]) => destpath.includes('/105-106/'))
      ).toHaveLength([...samplesAt105x5, ...samplesAt106x5].length);

      // check that the src and dest paths line up..
      expect(
        sortedList.find(
          ([srcpath, destpath]) =>
            srcpath === 'src/0sample.wav' &&
            destpath === 'masterpath/bpm/100-104/0sample.wav'
        )
      ).not.toBeUndefined();
      expect(
        sortedList.find(
          ([srcpath, destpath]) =>
            srcpath === 'src/0sample.wav' &&
            destpath === 'masterpath/bpm/140-142/0sample.wav'
        )
      ).not.toBeUndefined();
    });

    it('should sort out loops accordingly', () => {
      const samplesAt70Loopsx99 = [...Array(99)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 70,
        hasBpm: true,
        isLoop: true,
      }));
      const samplesAt71NonLoopsx101 = [...Array(101)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 71,
        hasBpm: true,
        isLoop: false,
      }));
      const fileAnalysisList = [
        ...samplesAt70Loopsx99,
        ...samplesAt71NonLoopsx101,
      ];

      const bpmLoopSort = new BpmSorter('masterpath', {
        includeLoops: true,
        includeNonLoops: false,
      });
      let sortedList = bpmLoopSort.sort(fileAnalysisList);

      expect(sortedList).toHaveLength([...samplesAt70Loopsx99].length);

      const bpmEtcSort = new BpmSorter('masterpath', {
        includeLoops: false,
        includeNonLoops: true,
      });
      sortedList = bpmEtcSort.sort(fileAnalysisList);
      expect(sortedList).toHaveLength([...samplesAt71NonLoopsx101].length);
    });
  });
});
