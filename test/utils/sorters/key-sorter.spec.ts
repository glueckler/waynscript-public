import { KeySorter } from '../../../app/utils/sorters/KeySorter';

describe('KeySorter class', () => {
  describe('sort() method', () => {
    it('should sort this analysis list correctly..', () => {
      const samplesAt70InAbx99 = [...Array(99)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 70,
        key: 'Ab',
        hasBpm: true,
        hasKey: true,
      }));
      const samplesAt71InAbx101 = [...Array(101)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 71,
        key: 'Ab',
        hasBpm: true,
        hasKey: true,
      }));

      const fileAnalysisList = [...samplesAt70InAbx99, ...samplesAt71InAbx101];
      const keysort = new KeySorter('masterpath', {
        includeLoops: true,
        includeNonLoops: true,
      });
      const sortedList = keysort.sort(fileAnalysisList);

      expect(sortedList.length).toBe(fileAnalysisList.length);
      expect(
        sortedList.filter(([, destpath]) => destpath.includes('/Ab/'))
      ).toHaveLength([...samplesAt70InAbx99, ...samplesAt71InAbx101].length);

      // check that the src and dest paths line up..
      expect(
        sortedList.find(
          ([srcpath, destpath]) =>
            srcpath === 'src/0sample.wav' &&
            destpath === 'masterpath/key/Ab/0sample.wav'
        )
      ).not.toBeUndefined();
      expect(
        sortedList.find(
          ([srcpath, destpath]) =>
            srcpath === 'src/0sample.wav' &&
            destpath === 'masterpath/key/Ab/0sample.wav'
        )
      ).not.toBeUndefined();
    });

    it('should sort out loops accordingly', () => {
      const samplesAt70InAbLoopsx99 = [...Array(99)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 70,
        key: 'Ab',
        hasBpm: true,
        hasKey: true,
        isLoop: true,
      }));
      const samplesAt71InAbNotLoopsx101 = [...Array(101)].map((_, index) => ({
        filename: `${index}sample.wav`,
        filepath: `src/${index}sample.wav`,
        bpm: 71,
        key: 'Ab',
        hasBpm: true,
        hasKey: true,
        isLoop: false,
      }));
      const fileAnalysisList = [
        ...samplesAt70InAbLoopsx99,
        ...samplesAt71InAbNotLoopsx101,
      ];

      const bpmLoopSort = new KeySorter('masterpath', {
        includeLoops: true,
        includeNonLoops: false,
      });
      let sortedList = bpmLoopSort.sort(fileAnalysisList);

      expect(sortedList).toHaveLength([...samplesAt70InAbLoopsx99].length);

      const bpmEtcSort = new KeySorter('masterpath', {
        includeLoops: false,
        includeNonLoops: true,
      });
      sortedList = bpmEtcSort.sort(fileAnalysisList);
      expect(sortedList).toHaveLength([...samplesAt71InAbNotLoopsx101].length);
    });
  });
});
