import { OneshotSorter } from '../../../app/utils/sorters/OneshotSorter';

describe('OneshotSorter class', () => {
  describe('sort() method', () => {
    it('should sort this analysis list correctly..', () => {
      const kicksamplesx99 = [...Array(99)].map((_, index) => ({
        filename: `${index}sample kick.wav`,
        filepath: `src/${index}sample kick.wav`,
        isOneshot: true,
        oneshotType: 'kick',
      }));
      const snaresamplesx101 = [...Array(101)].map((_, index) => ({
        filename: `${index}sample snare.wav`,
        filepath: `src/${index}sample snare.wav`,
        isOneshot: true,
        oneshotType: 'snare',
      }));

      const fileAnalysisList = [...kicksamplesx99, ...snaresamplesx101];
      const keysort = new OneshotSorter('masterpath');
      const sortedList = keysort.sort(fileAnalysisList);

      expect(sortedList.length).toBe(fileAnalysisList.length);
      expect(
        sortedList.filter(([, destpath]) => destpath.includes('/kick/'))
      ).toHaveLength([...kicksamplesx99].length);

      expect(
        sortedList.filter(([, destpath]) => destpath.includes('/snare/'))
      ).toHaveLength([...snaresamplesx101].length);

      // check that the src and dest paths line up..
      expect(
        sortedList.find(
          ([srcpath, destpath]) =>
            srcpath === 'src/0sample kick.wav' &&
            destpath === 'masterpath/oneshots/kick/0sample kick.wav'
        )
      ).not.toBeUndefined();
      expect(
        sortedList.find(
          ([srcpath, destpath]) =>
            srcpath === 'src/0sample snare.wav' &&
            destpath === 'masterpath/oneshots/snare/0sample snare.wav'
        )
      ).not.toBeUndefined();
    });
  });
});
